package Sorting;

public class InsertionSort_Simple {
    public void sort(int[] arr){

        int l = arr.length;
        for(int i = 1; i < l; i++){
            for(int j = i - 1; j >= 0; j--){
                if(arr[j] > arr[j+1]) swap(arr, j+1, j);
                else break;
            }
        }
    }

    private void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
