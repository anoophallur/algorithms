package Sorting;

public class MergeSort_simple {

    private int[] arr;

    public void sort(int[] arr){
        this.arr = arr;
        mergeSort(0, arr.length - 1);
    }

    private void mergeSort(int s, int e) {
        if(e <= s)return;
        int mid = s + (e-s)/2;
        mergeSort(s, mid);
        mergeSort(mid + 1, e);
        merge(s,mid,mid+1,e);
    }

    private void merge(int s1, int e1, int s2, int e2) {
        int[] temp1 = new int[e1-s1+1];
        int[] temp2 = new int[e2-s2+1];
        for(int i=s1;i<=e1;i++)temp1[i-s1]=arr[i];
        for(int i=s2;i<=e2;i++)temp2[i-s2]=arr[i];
        int a=s1,b=s2;
        for(int i=s1; i<=e2; i++){
            if(a>e1){
                while(b<=e2)arr[i++] = temp2[b++ - s2];
                break;
            }
            else if(b>e2){
                while(a<=e1)arr[i++] = temp1[a++ - s1];
                break;
            }
            else if(temp1[a-s1] <= temp2[b-s2]) arr[i] = temp1[a++ - s1];
            else if(temp1[a-s1] > temp2[b-s2]) arr[i] = temp2[b++ - s2];
        }
    }
}
