package Sorting;

import Sorting.Exceptions.ArrayLengthMismatchException;

public class QuickSort<T extends Comparable<? super T>> implements Sort<T> {
    private T[] input;
    @Override
    public void sortAscending(T[] input) throws ArrayLengthMismatchException {
        this.input = input;
        sortHelper(0, input.length - 1);
    }
    private void sortHelper(int lower, int upper) {
        if(upper - lower < 1)
            return;
        int pivot = partition(lower,upper);
        sortHelper(lower,pivot-1);
        sortHelper(pivot+1,upper);
    }
    private int partition(int lower, int upper) {
        int i = lower - 1;
        T pivot = input[upper];
        for(int j = lower ; j <= upper; j++){
            if(input[j].compareTo(pivot) < 0){
                i = i + 1;
                T temp = input[i];
                input[i] = input[j];
                input[j] = temp;
            }
        }
        i++;
        T temp = input[i];
        input[i] = input[upper];
        input[upper] = temp;
        return i;
    }
}
