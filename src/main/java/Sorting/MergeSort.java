package Sorting;

import Sorting.Exceptions.ArrayLengthMismatchException;

public class MergeSort<T extends Comparable<? super T>> implements Sort<T> {
    private T[] input;
    @Override
    public void sortAscending(T[] input) throws ArrayLengthMismatchException {
        this.input = input;
        sortHelper(0,input.length-1);
    }

    private void sortHelper(int lower, int upper) {
        if(upper - lower < 1) return;
        int mid = (upper + lower)/2;
        sortHelper(0,mid);
        sortHelper(mid+1,upper);
        merge(lower,mid,mid+1,upper);
    }

    private void merge(int lower1, int upper1, int lower2, int upper2) {
        T[] in1 = (T[]) new Comparable[upper1-lower1+1];
        T[] in2 = (T[]) new Comparable[upper2-lower2+1];
        System.arraycopy(input,lower1,in1,0,upper1-lower1+1);
        System.arraycopy(input,lower2,in2,0,upper2-lower2+1);
        int j=0,k=0;
        for(int i = lower1 ; i <= upper2 ;i++){
            if(in1.length <= j) {
                System.arraycopy(in2,k,input,i, upper2-i+1);
                break;
            } else if(in2.length <= k) {
                System.arraycopy(in1,j,input,i, upper2-i+1);
                break;
            } else if (in1[j].compareTo(in2[k]) < 0 ){
                input[i] = in1[j++];
            } else if(in1[j].compareTo(in2[k]) >= 0 ){
                input[i] = in2[k++];
            }
        }
    }
}
