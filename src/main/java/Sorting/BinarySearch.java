package Sorting;

/**
 * Created by anoop on 12/30/14.
 */
public class BinarySearch {


    private int[] arr;

    public int search(int[] arr, int key){
        this.arr = arr;
        int l = arr.length;

        return searchUtil(0, l -1, key);
    }

    private int searchUtil(int s, int e, int key) {
        int mid = s + (e-s)/2;
        if(arr[mid] == key)return mid;

        if(e == s) return -1;

        if(arr[mid] > key) return searchUtil(s, mid-1, key);
        else return searchUtil(mid+1,e, key);
    }
}
