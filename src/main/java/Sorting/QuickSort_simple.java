package Sorting;

import java.util.Stack;

public class QuickSort_simple {

    private int[] arr;

    public void sort(int[] arr){
        this.arr = arr;
        quickSort(0,arr.length-1);
    }

    private void quickSort(int s, int e) {
        if(e <= s)return;
        int pivot = partition(s,e);
        quickSort(s,pivot-1);
        quickSort(pivot+1,e);
    }

    private int partition(int s, int e) {
        int x = arr[e];
        int i = s-1;
        for(int j=s; j<e; j++){
            if(arr[j] <= x){
                i++;
                if(i != j)
                    swap(i,j);
            }
        }
        swap(i+1,e);
        return i+1;
    }

    private void swap(int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }


    public void iterativeQuickSort(int[] arr){
        this.arr = arr;

        Stack<Integer> stack = new Stack<Integer>();

        stack.push(0);
        stack.push(arr.length-1);

        int l , h, pivot;
        while(!stack.isEmpty()){
            h = stack.pop();
            l = stack.pop();

            pivot = partition(l, h);
            if(pivot-1 > l){
                stack.push(l);
                stack.push(pivot-1);
            }else if(pivot + 1 < h){
                stack.push(l+1);
                stack.push(h);
            }
        }
    }
}
