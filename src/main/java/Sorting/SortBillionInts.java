package Sorting;

import java.io.*;

// Incomplete. . just splitting the file now.

// The idea is to split the file into smaller chunks(so that they can be sorted in memory);
// Sort each of them,then take 2 at a time, do the merge step of the merge sort.
// If we split into 'm' files, after first pass, we would have ceil(m/2) files.after second pass ceil((ceil(m/2)/2) files ... and so on
// repeat the process till we have only 1 file

public class SortBillionInts {
    public void doSort(String inputFile, String outFilePrefix) throws IOException {

        FileReader fileReader = new FileReader(inputFile);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        int i=1,count=0;
        FileWriter fileWriter = new FileWriter(outFilePrefix + i);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        String line;
        while((line = bufferedReader.readLine()) != null){
            count++;
            bufferedWriter.write(line+"\n");
            if(count%10000000 == 0){
                i++;
                bufferedWriter.close();
                fileWriter.close();
                fileWriter = new FileWriter(outFilePrefix + i);
                bufferedWriter = new BufferedWriter(fileWriter);
            }
        }
        bufferedWriter.close();
        fileWriter.close();


    }
}
