package Sorting;

public class BubbleSort<T extends Comparable<? super T>> implements Sort<T> {
    @Override
    public void sortAscending(T[] input) {
        for(int i = 1; i <= input.length; i++){
            for(int j = 0; j < input.length - i; j++){
                if(input[j].compareTo(input[j+1]) > 0){
                    T temp = input[j];
                    input[j] = input[j+1];
                    input[j+1] = temp;
                }
            }
        }
    }
}
