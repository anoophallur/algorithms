package Sorting;

import Sorting.Exceptions.ArrayLengthMismatchException;

public interface Sort<T extends Comparable> {

    // Does Inplace sorting for this
    public void sortAscending(T[] input) throws ArrayLengthMismatchException;
}
