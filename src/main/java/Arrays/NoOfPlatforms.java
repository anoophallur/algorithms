package Arrays;

public class NoOfPlatforms {

    public int findNumber(int[] arr, int[] dep){
        int l1 = arr.length;
        int l2 = dep.length;

        int l = l1+l2;
        int[] all = new int[l];
        int[] idx = new int[l];

        int i=0,j=0;
        for(;i<l1;i++){all[i] = arr[i]; idx[i] = 1;}
        for(;j<l2;i++,j++){all[i] = dep[j]; idx[i] = -1;}

//        new QuickSortUtil().sort(all, idx);
        new MergeSortUtil().sort(all, idx);
        int max=0,sum=0;
        for(i=0;i<l;i++){
            sum+=idx[i];
            if(sum > max){max=sum;}
        }
        return max;
    }

    private class QuickSortUtil {
        private int[] all;
        private int[] idx;
        private void sort(int[] all, int[] idx) {
            this.all = all;
            this.idx = idx;
            quickSort(0,all.length-1);
        }

        private void quickSort(int s, int e) {
            if(e <= s)return;

            int pivot = partition(s,e);
            quickSort(s,pivot-1);
            quickSort(pivot+1,e);
        }

        private int partition(int s, int e) {
            int x = all[e];
            int i = s-1;
            for(int j=s;j<=e-1;j++){
                if(all[j] <= x){
                    i++;
                    swap(j,i);
                }
            }
            swap(i+1,e);
            return (i+1);
        }

        private void swap(int a, int b) {
            int temp = all[a];
            all[a] = all[b];
            all[b] = temp;

            temp = idx[a];
            idx[a] = idx[b];
            idx[b] = temp;

        }
    }

    private class MergeSortUtil {
        private int[] all;
        private int[] idx;
        private void sort(int[] all, int[] idx) {
            this.all = all;
            this.idx = idx;
            mergeSort(0,all.length-1);
        }

        private void mergeSort(int s, int e) {
            if(e<=s)return;
            int mid = (s+e)/2;
            mergeSort(s,mid);
            mergeSort(mid+1,e);
            merge(s,mid,mid+1,e);
        }

        private void merge(int s1, int e1, int s2, int e2) {
            int a=s1;
            int b=s2;
            int c=s1;

            int[] temp1 = new int[e1-s1+1];
            int[] temp2 = new int[e2-s2+1];
            int[] temp1a = new int[e1-s1+1];
            int[] temp2a = new int[e2-s2+1];
            for(int i = s1,j=0 ; i <= e1; i++,j++){
                temp1[j] = all[i];
                temp1a[j] = idx[i];
            }
            for(int i = s2,j=0 ; i <= e2; i++,j++){
                temp2[j] = all[i];
                temp2a[j] = idx[i];
            }
            for(int i = s1,j=0,k=0; i <= e2; i++){
                if(a>e1){
                    while(b <= e2){
                        all[c] = temp2[k];
                        idx[c] = temp2a[k];
                        c++;b++;k++;
                    }
                    break;
                }
                else if(b>e2){
                    while(a <= e1){
                        all[c] = temp1[j];
                        idx[c] = temp1a[j];
                        c++;a++;j++;
                    }
                    break;
                }
                else if(temp1[j] <= temp2[k]) {
                    all[c] = temp1[j];
                    idx[c] = temp1a[j];
                    c++;a++;j++;
                }
                else if(temp1[j] > temp2[k]) {
                    all[c] = temp2[k];
                    idx[c] = temp2a[k];
                    c++;b++;k++;
                }
            }
        }
    }
}
