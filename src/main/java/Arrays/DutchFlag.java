package Arrays;

public class DutchFlag {
    public void solve(int a[], int k){
        int f = 0, b = a.length-1, sb = -1,temp,temp1;
        while(f < b){
            if(a[f] < k){f++; continue;}
            else if(a[f] == k){
                if(sb == -1){sb = f;}
                f++;
                continue;
            }
            else{
                if(a[b] > k){b--; continue;}
                else if(a[b] == k){
                    temp = a[f];
                    a[f] = a[b];
                    a[b] = temp;
                    if(sb == -1)sb = f;
                    f++;
                    b--;
                    continue;
                } else if(a[b] < k){
                    if(sb == -1){
                        temp = a[f];
                        a[f] = a[b];
                        a[b] = temp;
                        sb = f;
                        f++;
                        b--;
                        continue;
                    } else{
                        temp = a[sb];
                        a[sb] = a[b];
                        temp1 = a[f];
                        a[f] = temp;
                        a[b] = temp1;
                        sb++;f++;
                        b--; continue;
                    }

                }
            }
        }
    }
}
