package Arrays;

public class LongestSubarray {
    public int findLength(int[] a){
        int prev = a[0];
        int s=0,e=0,len=1;
        int best_s=s,best_e=e,best_len = len;

        for(int i = 1; i < a.length;i++){
            if(a[i] == prev){
                e=i;
                len++;
                if(len > best_len){
                    best_len = len;
                    best_s=s;
                    best_e=e;
                }
            }else{
                s=i;e=i;len=1;
                prev=a[i];
            }
        }
//        for(int i = best_s ; i <= best_e;i++){
//            System.out.println(a[i]);
//        }
        return best_len;
    }
}
