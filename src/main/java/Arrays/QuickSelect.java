package Arrays;

/**
 * Created by anoop on 1/31/15.
 */
public class QuickSelect {
    int select(int[] arr, int k){
        return selecthelper(arr, k, 0, arr.length-1);
    }

    private int selecthelper(int[] arr, int k, int s, int e) {
        int pivot = partition(arr, s, e);
        if(k+s == pivot) return arr[pivot];
        else if(k+s < pivot) return selecthelper(arr, k, s, pivot-1);
        else return selecthelper(arr, k+s-pivot-1, pivot+1, e);
    }

    private int partition(int[] arr, int s, int e){
        int x = arr[e];
        int i = s-1;
        for(int j = s; j < e; j++){
            if(arr[j] < x){
                i++;
                if(i != j) swap(arr, i, j);
            }
        }
        i++;
        swap(arr, i, e);
        return i;
    }

    private void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
