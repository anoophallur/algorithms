package DP;

import java.util.ArrayList;

public class LongestIncreasingSubsequence {

    // naive implementation is O(2^n), not doing it here

    // http://stackoverflow.com/a/2631810/2151286

    // This one's time complexity is O(n^2). It will find any one of the longest increasing subsequence
    public int[] usingOnsquare(int[] arr) {
        ArrayList<Integer> res = new ArrayList<Integer>();

        int[] D = new int[arr.length];
        int[] prev = new int[arr.length];
        int best_match = -1, best_end = -1;
        D[0] = 1;
        prev[0] = -1;

        for(int i = 1 ; i < arr.length ; i++){
            D[i] = 1;
            prev[i] = -1;

            for(int j = i - 1; j >= 0; j--){
                if(D[j] + 1 > D[i] && arr[i] > arr[j]){
                    D[i] = D[j]+1;
                    prev[i] = j;
                }
            }
            if(D[i] > best_match){
                best_match = D[i];
                best_end = i;
            }
        }
        int d = best_end;
        while(d != -1){
            res.add(d);
            d = prev[d];
        }
        int[] res_array = new int[res.size()];
        for(int i = res.size()-1; i >= 0; i--){
            res_array[res.size()-1 - i] = arr[res.get(i)];
        }
        return res_array;
    }

    // Attempting O(nlgn) solution..
    // Still work in progress. . I hope I understand it later :(
    public int[] usingOnlgn(int[] arr){
        int l = arr.length;
        int[] S = new int[l];
        int[] T = new int[l];
        int[] parent = new int[l];

        // Initialize S array
        for(int i = 0 ; i < l ; i++){
            S[i] = -1;
        }

        S[0] = arr[0];
        T[0] = 0;
        parent[0] = -1;

        int length_till_now = 1;
        for(int i = 1; i < l ; i++){
            if(arr[i] > S[length_till_now-1]){
                // If X > last element in S, then append X to the end of S. This essentialy means we have found a new largest LIS.
                int now = length_till_now++;
                S[now] = arr[i];
                T[now] = i;
                parent[i] = T[now-1];
            }else{
                // Otherwise find the smallest element in S, which is >= than X, and change it to X

                // Doing binary search here
                int pos,low = 1, high = length_till_now;
                int mid = (low+high)/2;
                while(!((mid == 0 || S[mid-1] < arr[i]) && (S[mid] > arr[i] || S[mid] == -1))){
                    if(S[mid-1] < arr[i]){
                        low = mid+1;
                    }else{
                        high = mid-1;
                    }
                    mid = (low+high)/2;
                }
                pos = mid;

                // Updating arr[i] in that position
                S[pos] = arr[i];
                T[pos] = i;
                parent[i] = pos != 0 ? T[pos-1] : -1;

            }
        }
        int[] res = new int[length_till_now];

        return res;
    }
}
