package DP;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by anoop on 1/19/15.
 */
public class LongestPalindromicSubstring {
    Map<String, Integer> map;

    public LongestPalindromicSubstring() {
        this.map = new HashMap<String, Integer>();
    }

    // Geeks for geeks says time complexity is O(n^2) . . how the hell is this true. . explain please
    public int longestPalindromicSubstring(String str){
        if(map.containsKey(str))return map.get(str);
        int l = str.length();
        if(l <= 1) return l;
        int r1 = 0, r2, r3;
        if(str.charAt(0) == str.charAt(l-1))
            r1 = 2 + longestPalindromicSubstring(str.substring(1,l-1));
        r2 = longestPalindromicSubstring(str.substring(0, l-1));
        r3 = longestPalindromicSubstring(str.substring(1, l));

        int result = max(r1, r2, r3);
        map.put(str, result);
        return result;
    }

    private int max(int a, int b, int c){
        int r = a;
        if(b > r) r = b;
        if(c > r) r = c;
        return r;
    }
}
