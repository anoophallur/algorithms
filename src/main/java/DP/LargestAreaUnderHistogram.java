package DP;

import java.util.Stack;

public class LargestAreaUnderHistogram {

    public int calculate(int[] arr){
        int largest = 0;
        Stack<Integer> vals = new Stack<Integer>();
        Stack<Integer> idxs = new Stack<Integer>();

        for(int i = 0 ; i < arr.length; i++){
            if(vals.isEmpty() || arr[i] > vals.peek()){
                vals.add(arr[i]);
                idxs.add(i);
            }
            else if(arr[i] < vals.peek()){
                int last_start_idx = 0;
                while (!vals.isEmpty() && arr[i] < vals.peek()){
                    last_start_idx = idxs.pop();
                    int this_area = vals.pop() * (i - last_start_idx);
                    if(this_area > largest){
                        largest = this_area;
                    }
                }
                vals.add(arr[i]);
                idxs.add(last_start_idx);
            }
        }

        while (!vals.isEmpty()){
            int this_area = vals.pop() * (arr.length - idxs.pop());
            if(this_area > largest){
                largest = this_area;
            }
        }

        return largest;
    }
}
