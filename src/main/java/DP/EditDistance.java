package DP;

public class EditDistance {
    public int calculate(String in1, String in2){
        int l1 = in1.length();
        int l2 = in2.length();
        int[][] table = new int[l1+1][l2+1];
        int add, remove, substitute;

        for(int i = 0 ; i < l1+1 ; i++){
            for(int j = 0; j < l2 + 1; j++){

                // Degenerate cases
                if(i == 0 && j == 0){table[i][j] = 0; continue;}
                else if(i == 0){table[i][j] = j; continue;}
                else if(j == 0){table[i][j] = i; continue;}

                // Computing cost for addition, removal and substitution separately
                add = table[i-1][j] + 1;
                remove = table[i][j-1] + 1;
                substitute = table[i-1][j-1] + (in1.charAt(i-1) == in2.charAt(j-1) ? 0 : 1);

                // Pick the minimum of three
                table[i][j] = mininum(add, remove, substitute);
            }
        }

        return table[l1][l2];
    }

    private int mininum(int i1, int i2, int i3) {
        int smallest = i1;
        if(smallest > i2) smallest=i2;
        if(smallest > i3) smallest=i3;
        return smallest;
    }
}
