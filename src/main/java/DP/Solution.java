package DP;

import java.io.*;
import java.util.ArrayList;

public class Solution {

    /* Time complexity of the solution is O(n) */
    /* Auxillary Space complexity is O(n) as well */
    public static void main(String args[]) throws Exception {
        int[] arr = readInputArrray();

        /* sums is to keep track of the sum till that point of index in the array */
        ArrayList<Integer> sums = new ArrayList<Integer>();
        for (int i = 0; i < arr.length; i++) {
            sums.add(i + arr[i]);
        }

        /* result is the list to hold the final solution of result */
        ArrayList<Integer> result = new ArrayList<Integer>();
        /* Always start at index 0 */
        result.add(0);

        /* check base case */
        if (arr.length == 1) {
            printSolution(result);
        }

        while (true) {
            Integer current = result.get(result.size() - 1);
            Integer jumpValue = arr[current];

            /* check if solution is feasible in this iteration, by jumping over */
            if (arr.length - 1 - current <= jumpValue) {
                result.add(arr.length - 1);
                printSolution(result);
                break;
            } else {
                /* Update the current index, i.e get info about current position*/
                Integer maxIndex = current;

                /* Find the best possible jump */
                for (int i = current; i <= current + jumpValue; i++) {
                    if (maxIndex != 0 || sums.get(i) > sums.get(maxIndex)) {
                        maxIndex = i;
                    }
                }

                /* check whether we have reached last position by jumping */
                if (maxIndex.equals(current)) {
                    break;
                }
                result.add(maxIndex);
            }
        }

        /* If no solutions exist, just print failure */
        if(result.size() == 1){
            System.out.println("failure");
            return;
        }
    }

    /* Reads from stdin and returns the input array */
    private static int[] readInputArrray() throws IOException {
        /* Buffered Reader for reading from stdin*/
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        /* Using ArrayList since size of input array is not known beforehand
        while reading from stdin*/
        ArrayList<Integer> values = new ArrayList<Integer>();


        String line;
        while ((line = br.readLine()) != null) {
            values.add(Integer.parseInt(line));
        }

        /* Converting ArrayList to array before proceeding to solve the problem*/
        /* Converting to array because indexing is slightly faster on arrays than arraylist*/

        int[] list = new int[values.size()];
        for(int i = 0 ; i < values.size();i++){
            list[i] = values.get(i);
        }
        return list;
    }

    /* Helper function to print the solution */
    private static void printSolution(ArrayList<Integer> moves) {
        for(int i = 0 ; i < moves.size(); i++){
            System.out.print(moves.get(i)+", ");
        }
        System.out.println("out");
    }
}