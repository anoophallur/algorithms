package DP;

/**
 * Created by anoop on 1/19/15.
 */
public class RodCutting {

    private int[] prices;
    private int[] store;

    // Runs in O(n^2)
    int findMaxPrice(int[] prices, int length) {
        this.prices = prices;
        int l = prices.length;
        store = new int[length+1];
        for (int i = 0; i <= length; i++) {
            store[i] = -1;
        }
        return helper(length);
    }

    private int helper(int length) {
        if (store[length] != -1) return store[length];

        Integer result = Integer.MIN_VALUE;
        Integer temp;
        if(length == 0){
            result = 0;
        }else if (length == 1) {
            result = prices[0];
        } else {
            for (int i = 0; i < length; i++) {
                temp = prices[i] + helper(length - i - 1);
                if (temp > result)
                    result = temp;
            }
        }
        store[length] = result;
        return result;

    }
}
