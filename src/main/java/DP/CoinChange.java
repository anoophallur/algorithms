package DP;

import java.util.HashMap;

/**
 * Created by anoop on 12/30/14.
 */
public class CoinChange {
    private int[] d;

    // memoizing . . the values
    private HashMap<Combo, Integer> map = new HashMap<Combo, Integer>();

    // assume denominations is in sorted (decreasing) order

    public int numberOfCombinations(int value, int[] denominations){
        d = denominations;
        return helper(value, 0, denominations.length-1);
    }

    private int helper(int value, int s, int e) {
        Combo key = new Combo(value, s, e);
        if(map.containsKey(key)) return map.get(key);

        if(value == 0) return 1;
        if(s == e){
            return value % d[e] == 0 ? 1 : 0;
        }

        int ways = 0;
        for(int i = 0 ; value - i*d[s] >= 0; i++){
            ways += helper(value - i*d[s], s+1, e);
        }
        map.put(key, ways);
        return ways;
    }

    private class Combo {
        int value;
        int s;
        int e;

        private Combo(int value, int s, int e) {
            this.value = value;
            this.s = s;
            this.e = e;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Combo)) return false;

            Combo combo = (Combo) o;

            if (e != combo.e) return false;
            if (s != combo.s) return false;
            if (value != combo.value) return false;

            return true;
        }
    }
}
