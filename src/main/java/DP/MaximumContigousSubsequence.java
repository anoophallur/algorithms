package DP;

// See test case to figure out how this works
public class MaximumContigousSubsequence {

    // O(n^3) .. avoid
    public int[] usingNaiveApproach(int[] arr){
        int l = arr.length;
        int start = 0, end = l-1;
        int best_start = start, best_end = end, sum;
        int best_sum = Integer.MIN_VALUE;
        for(int i = 1 ; i <= l ; i++){
            for(int j = 0; j < l ; j++){
                start = j;
                end = j+i-1;
                sum = 0;
                if(end > l-1){
                    break;
                }
                for(int k = start; k <= end; k++){
                    sum += arr[k];
                }
                if(sum > best_sum){
                    best_sum = sum;
                    best_start = start;
                    best_end = end;
                }
            }
        }

        start = best_start;
        end = best_end;
        int[] ret = new int[end-start+1];
        for(int i = start,j=0 ; i <= end ;i++,j++){
            ret[j] = arr[i];
        }
        return ret;
    }

    // O(n) time, but some hacks. . see MIN_VALUE + 1000 for ex. If we leave it as MIN_VALUE, MIN_VALUE+(-2) bevomes +ve,
    // caused underflow.
    public int[] usingDP(int[] arr){
        int l = arr.length;
        int start = 0, end = 0;
        int i1,i2,this_best,best_start=0, best_end=0;
        int prev_best = Integer.MIN_VALUE+1000,best = Integer.MIN_VALUE+1000;
        for(int i = 0 ; i < l ; i++){
            i1 = arr[i];
            i2 = prev_best + i1;
            this_best = Math.max(i1, i2);
            prev_best = this_best;
            if(i1 > i2){start = i; end = i;}
            else {end = i;}
            System.out.println(this_best);
            if(this_best > best){
                best = this_best;
                best_start = start;
                best_end = end;
            }
        }
        start = best_start;
        end = best_end;
        int[] ret = new int[end-start+1];
        for(int i = start,j=0 ; i <= end ;i++,j++){
            ret[j] = arr[i];
        }
        return ret;

    }
}
