package DP;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by anoop on 1/19/15.
 */
public class EggDrop {

    private class Query {
        int n;
        int top;
        int bot;

        private Query(int n, int top, int bot) {
            this.n = n;
            this.top = top;
            this.bot = bot;
        }

        @Override
        public String toString() {
            return "Query{" +
                    "n=" + n +
                    ", top=" + top +
                    ", bot=" + bot +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Query)) return false;

            Query query = (Query) o;

            if (bot != query.bot) return false;
            if (n != query.n) return false;
            if (top != query.top) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = n;
            result = 31 * result + top;
            result = 31 * result + bot;
            return result;
        }
    }

    // using bottom up DP
    Map<Query, Integer> map;

    // See here, this is the functionality of this class
    // k : num_floors, n : num_eggs 0
    // Time complexity ==> O(k*nk)  ; k for the for loop, nk for each calculation within the loop ? mostly
    public int eggDrop(int num_eggs, int num_floors) {
        map = new HashMap<Query, Integer>();
        return eggDropHelper(num_eggs, num_floors, 1);
    }

    private int eggDropHelper(int num_eggs, int top, int bot) {
        Query query = new Query(num_eggs, top, bot);
        if (map.containsKey(query)) return map.get(query);

        Integer result = Integer.MAX_VALUE;
        if (bot > top) result = 0;
        else if (bot == top) result = 1;
        else if (num_eggs == 1) result = top - bot + 1;
        else {
            System.out.println(query);
            Integer temp;
            for (int i = bot; i <= top; i++) {
                // max to consider the worst case, i.e one may be lower, but we have to account for worst case
                temp = 1 + Math.max(eggDropHelper(num_eggs - 1, i - 1, bot), eggDropHelper(num_eggs, top, i + 1));
                if (temp < result)
                    result = temp;
            }
        }
        map.put(query, result);
        return result;
    }
}
