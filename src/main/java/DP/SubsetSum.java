package DP;

/**
 * Created by anoop on 1/19/15.
 */
public class SubsetSum {
    private int[] arr;

    // O(sum*n) ?? how the hell is this true
    public boolean isPossible(int[] arr, int target){
        this.arr = arr;
        return helper(0, arr.length-1, target);
    }

    private boolean helper(int s, int e, int target) {
        if(target == 0)return true;
        if(s == e && arr[s] != target) return false;
        return helper(s+1, e, target-arr[s]) || helper(s+1, e, target);
    }
}
