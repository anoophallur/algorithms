package DP;

import java.util.*;

class SnakeLadder {
    int[] grid = new int[37];
    boolean[] visited = new boolean[37];

    public SnakeLadder() {
        grid[2] = 15;
        grid[5] = 7;
        grid[9] = 27;
        grid[18] = 29;
        grid[25] = 35;
        grid[34] = 12;
        grid[32] = 30;
        grid[24] = 16;
        grid[20] = 6;
        grid[17] = 4;
    }
    class Entry{
        int pos;
        int distance;
        Entry(int pos,int distance){
            this.pos = pos;
            this.distance = distance;
        }
    }
    public List<Integer> solve() {
        visited[1] = true;

        LinkedList<Entry> queue = new LinkedList<Entry>();
        queue.add(new Entry(1,0));
        Entry n = null;
        while (!queue.isEmpty()) {
            n = queue.remove();
            if (n.pos == 36) {
                break;
                //I have reached the destination
            }
            for (int i = n.pos + 1; i <= n.pos + 6 &&  i <= 36; i++) {
                if (!visited[i]) {
                    visited[i] = true;
                    Entry entry = new Entry(0, n.distance + 1);
                    if(grid[i] != 0){
                        entry.pos = grid[i];
                    } else {
                        entry.pos = i;
                    }
                    queue.push(entry);
                }
            }
        }

        System.out.println(n.distance);
        return new ArrayList<Integer>();
    }
}
