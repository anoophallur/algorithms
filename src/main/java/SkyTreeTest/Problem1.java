/**
 * Created by anoop on 3/8/15.
 */
public class Problem1 {

    private static final int ALPHABET_SIZE = 26;

    public static void main(String[] args) {
        Problem1 problem = new Problem1();
        String input;

        input = "A slow yellow fox crawls under the proactive dog";
//        input = "A quick brown fox jumps over the lazy dog";
//        input = "Lions, and tigers, and bears, oh my!";
//        input = "";


        String result = problem.solve(input);
        System.out.println(result);
    }

    private String solve(String input) {

        // return null for invalid input
        if(input == null) return null;

        // Boolean array to represent presence of specific characters, default initialization to false
        boolean[] presence = new boolean[ALPHABET_SIZE];
        StringBuilder sb = new StringBuilder();
        int length = input.length();
        char c;

        input = input.toLowerCase();
        for(int i = 0 ; i < length ; i++){
            // deal only with lowercase characters
            c = input.charAt(i);
            if(c >= 'a' && c <= 'z')
                presence[c-'a'] = true;
        }
        for(int i = 0 ; i < ALPHABET_SIZE ;i++){
            // find missing characters
            if(!presence[i])
                sb.append((char)('a'+i));
        }
        return sb.toString();
    }
}
