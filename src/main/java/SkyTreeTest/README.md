There are two files Problem1.java and Problem2.java corresponding to the two problems given.

To compile and solve the problems for a particular input, uncomment the corresponding input line in the file,
ex.  input = "A slow yellow fox crawls under the proactive dog"; is commented out. Please uncomment your required
input / add new input

With JDK7 or higher, on a linux machine, to compile

$ javac Problem1.java
$ java Problem1.class

similarly for Problem2

The output is printed to stdout. Inputs are not taken in from stdin , rather they are in problem itself because it is
easier to test and run the code. Note that for some of the inputs of Problem, output is blank line, which means nothing
is seen, this is the expected behaviour