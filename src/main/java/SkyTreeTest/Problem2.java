import java.util.ArrayList;
import java.util.List;

/**
 * Created by anoop on 3/8/15.
 */

public class Problem2 {

    public static void main(String[] args) {
        List<String> result;
        Problem2 problem = new Problem2();

        result = problem.solve(2, "..R....");
//        result= problem.solve(3,  "RR..LRL");
//        result= problem.solve(2,  "LRLR.LRLR");
//        result= problem.solve(10,  "RLRLRLRLRL");
//        result= problem.solve(1,  "...");
//        result= problem.solve(1,  "LRRL.LR.LRR.R.LRRL.");

        for (int i = 0; i < result.size(); i++) {
            System.out.println(result.get(i));
        }
    }


    public List<String> solve(int speed, String line) {
        // Particle movement can be represented by shifting bits left or right.
        // Can use binary with bitwise operations to represent these arrays of particles.
        long leftBits = stringToBinary(line, 'L');
        long rightBits = stringToBinary(line, 'R');
        long state = leftBits | rightBits;

        int size = line.length();
        // mask enforces length of tube
        long mask = (long) Math.pow(2.0, size) - 1;
        ArrayList<String> result = new ArrayList<String>();
        while (state > 0) {
            result.add(animateLine(state, size));
            leftBits <<= speed;
            rightBits >>= speed;
            state = (leftBits | rightBits) & mask;
        }
        result.add(animateLine(state, size));
        return result;
    }

    protected String animateLine(long state, int size) {
        StringBuilder line = new StringBuilder();
        while (state > 0) {
            // check whether odd to determine rightmost digit
            if (state % 2 == 1) {
                line.append("X");
            } else {
                line.append(".");
            }
            state /= 2;
        }
        while (line.length() < size) {
            line.append(".");
        }
        return line.reverse().toString();
    }

    // extract the particles of one direction from init as N-length binary,
    // where N is the number of positions (length) of line
    protected long stringToBinary(String line, char direction) {
        int exponent = line.length() - 1;
        long value = 0L;
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == direction) {
                // record as 2 ** exponent
                value += Math.pow(2.0, exponent - i);
            }
        }
        return value;
    }
}