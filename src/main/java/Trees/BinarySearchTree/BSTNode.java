package Trees.BinarySearchTree;

public class BSTNode<T extends Comparable> {
    public T data;
    public BSTNode<T> left;
    public BSTNode<T> right;

    public int level;


    public BSTNode(T data) {
        this.data = data;
        this.level = 0;
    }

    private void addRightChild(BSTNode<T> node) {
        if (this.right == null) {
            node.level = this.level+1;
            this.right = node;
        } else {
            right.addElement(node);
        }
    }

    private void addLeftChild(BSTNode<T> node) {
        if (this.left == null) {
            node.level = this.level+1;
            this.left = node;
        } else {
            left.addElement(node);
        }
    }

    public void addElement(BSTNode<T> node) {
        if (node.data.compareTo(data) > 0) {
            addRightChild(node);
        } else if (node.data.compareTo(data) < 0) {
            addLeftChild(node);
        }

    }

    public void inOrderTraverse() {
        if(left != null){
            left.inOrderTraverse();
        }
        System.out.println("  "+data+"  ");
        if(right != null){
            right.inOrderTraverse();
        }

    }

    public boolean search(T element) {
        if (data == element) {
            return true;
        }else {
            if (left != null) return left.search(element);
            else if (right != null) return right.search(element);
            else return false;
        }
    }

    private int leftDepth(){
        if(left == null) return 0;
        else return left.maxDepth();
    }

    private int rightDepth(){
        if(right == null) return 0;
        else return right.maxDepth();
    }

    public int maxDepth() {
        return (1 + Math.max(leftDepth(), rightDepth()));
    }
}
