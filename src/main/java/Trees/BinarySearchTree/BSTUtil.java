package Trees.BinarySearchTree;

/**
 * Created by anoop on 12/6/14.
 */

// Have to write tests for this class
public class BSTUtil {

    // Ideally this should receive a tree object, but managing with root node for now
    public <T extends Comparable> boolean checkValidBST(BSTNode<T> root){

        return isValid(root, null, null);
    }

    private <T extends Comparable<T>> boolean isValid(BSTNode<T> node, T min, T max) {
        if(node.left != null){
            if((min != null && node.data.compareTo(min) < 0) || !isValid(node.left, min, node.data))
                return false;
        }
        if(node.right != null){
            if((max != null && node.data.compareTo(max) > 0) || !isValid(node.right, node.data, max))
                return false;
        }
        return true;
    }
}
