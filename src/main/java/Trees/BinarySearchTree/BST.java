package Trees.BinarySearchTree;

import Trees.BinarySearchTree.Exceptions.RootAlreadySetException;

import java.util.LinkedList;
import java.util.Queue;

public class BST<T extends Comparable>{
    public BSTNode<T> root;

    public  BST(){
        this.root = null;
    }

    public  BST(BSTNode<T> root){
        this.root = root;
    }

    public void setRoot(BSTNode<T> root) throws RootAlreadySetException {
        if(this.root == null)
            this.root = root;
        else throw new RootAlreadySetException();
    }

    // For now assume that equal values will not be added to the BST
    public void addElement(BSTNode<T> node) throws RootAlreadySetException {
        if(this.root == null){
            setRoot(node);
        }
        root.addElement(node);
    }

    public void removeElementWithValue(T value){
        if(root.data == value && root.left == null && root.right == null){
            root = null;
        } else {
            BSTNode<T> parent = root;
            BSTNode<T> child = root;
            int direction = 0; // 1 if child is rt of parent, -1 if left
            while (value.compareTo(child.data) != 0){
                parent = child;
                int comparisionValue = value.compareTo(child.data);
                direction = comparisionValue;
                if(comparisionValue > 0){
                    child = parent.right;
                } else if (comparisionValue < 0){
                    child = parent.left;
                }
            }
            // Now child will be pointing to the node to be deleted and parent to its parent

            // Node to be deleted is right child of parent
            if(direction == 1){
                if(child.left == null && child.right == null){parent.right = null;}
                if(child.right == null){
                    parent.right = child.left;
                } else if(child.left == null){
                    parent.right = child.right;
                }

                // Find minimum value in right sub tree
                BSTNode<T> minChildsParent = child;
                BSTNode<T> minChild = child.right;
                if(minChild.left != null){
                    minChildsParent = minChild;
                    minChild = minChild.left;
                }
                child.data = minChild.data;
                if(minChildsParent == child){
                    minChildsParent.right = minChild.right;
                } else {
                    minChildsParent.left = minChild.right;
                }

            }

            // Node to be deleted is left child of parent
            else if(direction == -1){
                if(child.left == null && child.right == null){parent.left = null;}
                if(child.right == null){
                    parent.left = child.left;
                } else if(child.left == null){
                    parent.left = child.right;
                }

                BSTNode<T> minChildsParent = child;
                BSTNode<T> minChild = child.right;
                if(minChild.left != null){
                    minChildsParent = minChild;
                    minChild = minChild.left;
                }
                child.data = minChild.data;
                if(minChildsParent == child){
                    minChildsParent.right = minChild.right;
                } else {
                    minChildsParent.left = minChild.right;
                }
            }
        }
    }

    public void inOrderTraverse(){
        root.inOrderTraverse();
    }

    public boolean search(T element){
        return root.search(element);
    }

    // This is actually used for level order printing
    public void bfsTraverse(){
        Queue<BSTNode<T>> queue = new LinkedList<BSTNode<T>>();
        queue.add(root);
        int prevLevel = root.level;
        while(queue.size() != 0){
            BSTNode<T> element = queue.remove();
            if(element.level > prevLevel){
                prevLevel = element.level;
                System.out.println("");
            }
            if(element.left != null) queue.add(element.left);
            if(element.right != null) queue.add(element.right);
            System.out.print(element.data + "  ");
        }
        System.out.println(" ");
    }

    public int maxDepth(){
        if (root == null)
            return 0;
        else
            return root.maxDepth();

    }
}
