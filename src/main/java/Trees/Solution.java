package Trees;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Solution{
    public String generateClass(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        // handle case if file is not found correctly.
        String line;
        StringBuilder sb = new StringBuilder();

        while((line = br.readLine()) != null){
            String[] splits = line.split(" ");
            if("private".equals(splits[0].trim())){
                String fieldName = splits[splits.length-1].trim();
                if(fieldName.charAt(fieldName.length()-1)==';'){
                    fieldName = fieldName.substring(0, fieldName.length() - 1);
                    String type = splits[splits.length-2].trim();
                    sb.append(printGetter(fieldName, type));
                    if(!"final".equals(splits[1])){
                        sb.append(printSetter(fieldName, type));
                    }
                }
            }
        }
        return sb.toString();
    }

    public String printSetter(String fieldName, String type){
        StringBuilder sb = new StringBuilder();
        String methodName = "set" + capitalizeFirstAlphabet(fieldName);
        sb.append("public void " + methodName + "(" + type + " " + fieldName + "){");
        sb.append("this." + fieldName + " = " + fieldName + ";");
        sb.append("}");
        return sb.toString();
    }

    private String printGetter(String fieldName, String type){
        StringBuilder sb = new StringBuilder();
        String methodName = "get" + capitalizeFirstAlphabet(fieldName);
        sb.append("public " + type + " "+methodName+"(){");
        sb.append("      "+"return "+type+";");
        sb.append("}");
        return sb.toString();
    }

    private String capitalizeFirstAlphabet(String word){
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }
}