package Trees.AVLBinarySearchTree;

import Trees.BinarySearchTree.BST;

public class AVLTree<T extends Comparable> extends BST {
    public AVLTreeNode<T> root;

    public AVLTree() {
        this.root = null;
    }

    public AVLTree(AVLTreeNode<T> root) {
        this.root = root;
    }

    public void setRoot(AVLTreeNode<T> root) {
        this.root = root;
    }

    public void insert(AVLTreeNode<T> avlTreeNode) {
        root.insert(avlTreeNode);
    }
}
