package Trees.AVLBinarySearchTree;

import Trees.BinarySearchTree.BSTNode;

public class AVLTreeNode<T extends Comparable> extends BSTNode<T>{
    public AVLTreeNode<T> left;
    public AVLTreeNode<T> right;
    public int balanceFactor;

    public AVLTreeNode(T data) {
        super(data);
        this.balanceFactor = calculateBalanceFactor();
    }

    private int calculateBalanceFactor() {
        return (leftDepth() - rightDepth());
    }

    private int leftDepth() {
        if (left == null) return 0;
        else return left.maxDepth();
    }

    private int rightDepth() {
        if (right == null) return 0;
        else return right.maxDepth();
    }

    public int maxDepth() {
        return (1 + Math.max(leftDepth(), rightDepth()));
    }

    public void insert(AVLTreeNode<T> avlTreeNode) {

        if (this.data.compareTo(avlTreeNode.data) > 0) {
            if (this.left == null) this.left = avlTreeNode;
            else left.insert(avlTreeNode);
        } else {
            if (this.right == null) this.right = avlTreeNode;
            else right.insert(avlTreeNode);
        }
        this.balanceFactor = calculateBalanceFactor();
        if (this.balanceFactor > 1 || this.balanceFactor < -1) {
            this.balance();
        }
    }

    private void balance() {

        if (balanceFactor < -1) {//left rotation
            this.leftRotate();
        }else if (balanceFactor > 1) {//right rotation
            this.rightRotate();
        }
        this.balanceFactor = calculateBalanceFactor();
        if (this.balanceFactor > 1 || this.balanceFactor < -1) {
            this.balance();
        }
    }

    private void rightRotate() {
        AVLTreeNode<T> presentLeft = this.left;
        AVLTreeNode<T> thisNodeCopy = new AVLTreeNode<T>(this.data);
        thisNodeCopy.right = this.right;
        thisNodeCopy.left = presentLeft.right;
        this.right = thisNodeCopy;

        this.data = presentLeft.data;
        this.left = presentLeft.right;
    }

    private void leftRotate() {
        AVLTreeNode<T> presentRight = this.right;
        AVLTreeNode<T> thisNodeCopy = new AVLTreeNode<T>(this.data);
        thisNodeCopy.left = this.left;
        thisNodeCopy.right = presentRight.left;
        this.left = thisNodeCopy;

        this.data = presentRight.data;
        this.right = presentRight.right;
    }
}
