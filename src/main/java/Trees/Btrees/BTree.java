package Trees.Btrees;

public class BTree<K extends Comparable<K>, V> {
    private final int order;
    private BTreeNode root;

    public BTree(int order) {
        this.order = order;
        root = null;
    }

    private class Entry{
        K key;
        V value;

        public Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    private class BTreeNode{
        Entry[] entires = (Entry[]) new Object[order];
        int filled = 0;

        private void addEntry(Entry entry){
            if(filled == order){
                //splitting and allocating a new node
            }
            entires[filled++] = entry;
        }
    }

    public void insert(K key, V value){
        Entry entry = new Entry(key, value);
        if(root == null){
            root = new BTreeNode();
        }
        root.addEntry(entry);
    }
}
