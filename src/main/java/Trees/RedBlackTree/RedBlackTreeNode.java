package Trees.RedBlackTree;

/**
 * Created by anoop on 1/17/15.
 */
public class RedBlackTreeNode<T extends Comparable> {
    T data;
    RedBlackTreeNode left;
    RedBlackTreeNode right;
    RedBlackTreeNode parent;
    boolean red; // default is black
}
