package Threading;

/**
 * Created by anoop on 3/28/15.
 */
public class OddEvenThreads {

    private Boolean prevoddPrint = false;

    private class OddRunnable implements Runnable {
        int odd = 1;

        @Override
        public void run() {
            while (true) {
                synchronized (prevoddPrint) {
                    if (!prevoddPrint) {
                        System.out.print(odd + " ");
                        odd += 2;
                        prevoddPrint ^= true;
                    }
                }
            }
        }
    }

    private class EvenRunnable implements Runnable {
        int even = 2;

        @Override
        public void run() {
            while (true) {
                synchronized (prevoddPrint) {
                    if (prevoddPrint) {
                        System.out.print(even + " ");
                        even += 2;
                        prevoddPrint ^= true;
                    }
                }
            }
        }
    }

    public void printOddEven() {
        Thread threadodd = new Thread(new OddRunnable());
        Thread threadeven = new Thread(new EvenRunnable());

        threadeven.run();
        threadodd.run();
//        try {
//            threadeven.join();
//            threadodd.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }
}
