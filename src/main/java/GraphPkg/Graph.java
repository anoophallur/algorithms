package GraphPkg;

import java.util.LinkedList;
import java.util.List;

public class Graph<T> {
    public GraphNode<T> root;

    public Graph() {
    }

    public Graph(GraphNode<T> root) {
        this.root = root;
    }

    public void setRoot(GraphNode<T> root) {
        this.root = root;
    }

    public void bfsTraverse(){
        List<GraphNode> queue = new LinkedList<GraphNode>();
        queue.add(root);

        while(!queue.isEmpty()){
            GraphNode<T> node = queue.remove(0);
            node.visit();
            for(GraphNode<T> link : node.links){
                if(!link.visited){
                    queue.add(link);
                }
            }
        }
    }
}
