package GraphPkg;

import java.util.ArrayList;
import java.util.List;

public class GraphNode<T> {
    private T value;
    public List<GraphNode<T>> links;
    public boolean visited;

    public GraphNode(T value) {
        this.value = value;
        links = new ArrayList<GraphNode<T>>();
        visited = false;
    }

    public void addLink(GraphNode<T> link){
        links.add(link);
        link.links.add(this);
    }

    public void visit() {
        visited = true;
        System.out.println(value);
    }
}
