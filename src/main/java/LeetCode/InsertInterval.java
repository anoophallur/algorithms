package LeetCode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anoop on 1/5/15.
 */
public class InsertInterval {
    static class Interval {
        int start;
        int end;

        public Interval() {
            start = 0;
            end = 0;
        }

        public Interval(int s, int e) {
            start = s;
            end = e;
        }

        @Override
        public String toString() {
            return "Interval{" +
                    "start=" + start +
                    ", end=" + end +
                    '}';
        }
    }

    public List<Interval> insert(List<Interval> intervals, Interval newInterval) {
        List<Interval> list = new ArrayList<Interval>();
        int start=0, end=0;
        int end_idx=0;
        boolean started = false, finished = false;
        for(int i = 0 ; i < intervals.size(); i++){
            Interval interval = intervals.get(i);

            if(newInterval.start > interval.end){
                list.add(interval);
                continue;
            }
            if(!started && (newInterval.start <= interval.end)){
                if(newInterval.start < interval.start) start = newInterval.start;
                else start = interval.start;
                started = true;
            }
            if(started && (newInterval.end <= interval.end)){
                if(interval.start > newInterval.end){
                    end = newInterval.end;
                    end_idx = i - 1;
                }
                else {
                    end = interval.end;
                    end_idx = i;
                }
                finished = true;
                list.add(new Interval(start, end));
                break;
            }
        }
        if(started){
            if(finished){
                for(int i = end_idx + 1; i< intervals.size(); i++){
                    list.add(intervals.get(i));
                }
            }else{
                end = newInterval.end;
                list.add(new Interval(start, end));
            }
        }else{
            list.add(newInterval);
        }
        return list;
    }
}
