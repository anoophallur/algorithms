package LeetCode;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by anoop on 3/14/15.
 */
public class MedianOfSortedArrays {
    public double findMedianSortedArrays(int A[], int B[]) {
        int e1 = A.length - 1;
        int e2 = B.length - 1;
        int delta = 0;

        if (e1 == -1) {
            return (e2 % 2 == 1) ? (B[e2 / 2] + B[e2 / 2 + 1]) / 2.0 : B[e2 / 2];
        } else if (e2 == -1) {
            return (e2 % 2 == 1) ? (A[e1 / 2] + A[e1 / 2 + 1]) / 2.0 : A[e1 / 2];
        } else if(e1 == 0 && e2 == 0) {
            return (A[0]+B[0])/2.0;
        }else {
//            if(e1 < 1){
//                A = new int[]{Integer.MIN_VALUE+1, A[0], Integer.MAX_VALUE-1};
//                e1 += 2;
//                delta++;
//            }
//            if(e2 < 1){
//                B = new int[]{Integer.MIN_VALUE-1, B[0], Integer.MAX_VALUE-1};
//                e2 += 2;
//                delta ++;
//            }
            if ((e1 + e2) % 2 == 0) {
                //even
                return (findKth(A, 0, e1, B, 0, e2, (e2 + e1) / 2 ) + findKth(A, 0, e1, B, 0, e2, (e2 + e1) / 2 + 1)) / 2.0;
            } else {
                //odd
                return findKth(A, 0, e1, B, 0, e2, (e2 + e1 + 1) / 2);
            }
        }
    }

    private double findKth(int[] A, int s1, int e1, int[] B, int s2, int e2, int k) {
        int amid, alen = e1 - s1 + 1;
        int bmid, blen = e2 - s2 + 1;

        amid = (s1 + e1) / 2;
        bmid = (s2 + e2) / 2;

        if(alen == 1){
            int temp = B[s2+k-1];
            if(A[s1] > temp)return temp;
            else return Math.max(A[s1], k!=0?B[s2+k-1]:Integer.MIN_VALUE);
        }

        if(blen == 1){
            int temp = A[s1+k];
            if(B[s2] > temp)return temp;
            else return Math.max(B[s2], k!=0?A[s1+k-1]:Integer.MIN_VALUE);
        }

        if (alen + blen <= 5) {
            ArrayList<Integer> temp = new ArrayList<Integer>(4);
            for (int i = s1; i <= e1; i++) {
                temp.add(A[i]);
            }
            for (int i = s2; i <= e2; i++) {
                temp.add(B[i]);
            }
            Collections.sort(temp);
            return temp.get(k);
        }

        if (A[amid] < B[bmid]) {
                k = k - (amid - s1);
                s1 = amid;
                e2 = bmid;
        } else {
                k = k - (bmid - s2);
                s2 = bmid;
                e1 = amid;
        }
        return findKth(A, s1, e1, B, s2, e2, k);
    }
}
