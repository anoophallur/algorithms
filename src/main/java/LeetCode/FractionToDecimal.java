package LeetCode;

import java.util.HashMap;

/**
 * Created by anoop on 1/1/15.
 */
public class FractionToDecimal {
    public String fractionToDecimal(int numerator, int denominator) {
        long n = Math.abs(numerator);
        long int_part = Math.abs(n / denominator);
        long rem = n % denominator;
        String sign="";
        if((numerator < 0 && denominator >= 0)||(numerator > 0 && denominator < 0) )sign="-";
        String res = String.format("%s%d", sign,int_part);

        if(rem == 0) return res;
        rem = Math.abs(rem);
        denominator = Math.abs(denominator);
        HashMap<Long, Integer> map = new HashMap<Long, Integer>();

        res += ".";
        String frac = "";

        Integer pos;
        rem = rem * 10;
        map.put(rem, 0);
        int i = 1;
        while(true){
            int_part = Math.abs(rem / denominator);
            frac += int_part;
            rem = (rem % denominator) * 10;
            if(rem == 0)break;
            if(map.containsKey(rem)){
                pos = map.get(rem);
                return res + frac.substring(0,pos) + "(" + frac.substring(pos, frac.length()) + ")";
            }
            map.put(rem, i);
            i++;
        }
        return res+frac;
        }
}
