package LeetCode;

/**
 * Created by anoop on 1/1/15.
 */
public class AddTwoNumbers {
    public class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
            next = null;
        }
    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int pow = 0;

        long n1 = 0;
        long n2 = 0;
        while (l1 != null) {
            n1 += l1.val * Math.pow(10, pow++);
            l1 = l1.next;
        }
        pow = 0;
        while (l2 != null) {
            n2 += l2.val * Math.pow(10, pow++);
            l2 = l2.next;
        }

        long sum = n1 + n2;
        if (sum == 0) return new ListNode(0);
        ListNode prev = null;
        ListNode first = null;
        ListNode node = null;
        int digit;

        while (sum != 0) {
            digit = (int) sum % 10;
            sum = sum / 10;
            node = new ListNode(digit);
            if (first == null) {
                first = node;
            }
            if (prev != null) prev.next = node;
            prev = node;
        }
        return first;
    }
}
