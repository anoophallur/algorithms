package LeetCode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by anoop on 3/3/15.
 */
public class MergeInterval {

    public static class Interval {
        int start;
        int end;

        Interval() {
            start = 0;
            end = 0;
        }

        Interval(int s, int e) {
            start = s;
            end = e;
        }
    }

    public List<Interval> merge(List<Interval> intervals){
        ArrayList<Interval> ans = new ArrayList<Interval>();
        if(intervals.size() == 0)return ans;
        Collections.sort(intervals, new Comparator<Interval>() {
            @Override
            public int compare(Interval o1, Interval o2) {
                return new Integer(o1.start).compareTo(new Integer(o2.start));
            }
        });
        boolean merged=false;
        Interval prev = intervals.get(0);
        for(int i = 1 ; i < intervals.size(); i++){
            merged = isMerged(prev, intervals.get(i));
            if(merged){
                Interval inter = new Interval(prev.start, Math.max(prev.end, intervals.get(i).end));
                prev = inter;
            } else {
                ans.add(prev);
                prev = intervals.get(i);
            }
        }
        ans.add(prev);
        return ans;
    }

    private boolean isMerged(Interval i1, Interval i2) {
        return (i2.start <= i1.end);
    }

    public static void main(String[] args) {
        MergeInterval mergeInterval = new MergeInterval();
        Interval interval1 = new Interval(1,4);
        Interval interval2 = new Interval(0,2);
        Interval interval3 = new Interval(3,5);

        ArrayList<Interval> intervals = new ArrayList<Interval>();
        intervals.add(interval1);
        intervals.add(interval2);
        intervals.add(interval3);

        List<Interval> merge = mergeInterval.merge(intervals);

        System.out.println(merge);

    }
}
