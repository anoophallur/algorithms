package LeetCode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anoop on 1/1/15.
 */
public class IPAddresses {
    public List<String> restoreIpAddresses(String s) {
        return helper(s, 4);
    }

    List<String> helper(String s,int n){
        List<String> res = new ArrayList<String>();
        int l = s.length();
        if(l == 0) return res;
        if(n == 0)return res;

        if(n == 1){
            if(l > 4) return res;
            else if(l > 0 && l < 4) {
                if(checkValid(s)) res.add(s);
                return res;
            }
        }

        if(l > 1){
            String prefix = s.substring(0,1);
            if(checkValid(prefix)){
                List<String> combos = helper(s.substring(1), n-1);
                for(String post : combos){
                    res.add(prefix + "." + post);
                }
            }
        }
        if(l > 2){
            String prefix = s.substring(0,2);
            if(checkValid(prefix)){
                List<String> combos = helper(s.substring(2), n-1);
                for(String post : combos){
                    res.add(prefix + "." + post);
                }
            }
        }
        if(l > 3){
            String prefix = s.substring(0,3);
            if(checkValid(prefix)){
                List<String> combos = helper(s.substring(3), n-1);
                for(String post : combos){
                    res.add(prefix + "." + post);
                }
            }
        }
        return res;
    }
    private boolean checkValid(String s){
        try{
            if(s == null || s.isEmpty()) return false;
            Integer num = Integer.parseInt(s);

            //if(num == 0 && s.length() != 1) return false;
            //else if(num < 10 && s.length() != 1 ) return false;
            //else if(num < 100 && s.length() != 2 ) return false;

            if(num < 256 && num >= 0) {
                if(num < 10 && s.length() != 1) return false;
                else if(num < 100 && s.length() != 2 ) return false;

                return true;
            }
        } catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return false;
    }
}
