package LeetCode;

/**
 * Created by anoop on 1/6/15.
 */
public class ReverseKNodesLinkedList {
    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
            next = null;
        }
    }

    public ListNode reverseKGroup(ListNode head, int k) {
        ListNode f = head, s = head, pre = null, headp = null, preend = null;
        boolean finished = false;
        while (s != null) {
            for (int i = 1; i <= k; i++) {
                if (s != null) {
                    pre = s;
                    s = s.next;
                } else {
                    break;
                }
                if (i == k) {
                    finished = true;
                }
            }
            if (finished) {
                pre.next = null;
                reverse(f);
                if (headp == null)
                    headp = pre;
                else
                    preend.next = pre;
                f.next = s;
                preend = f;
                f = s;
            }
        }
        if(headp == null)
            headp = head;
        return headp;
    }

    private ListNode reverse(ListNode n) {
        ListNode pre, current, next;
        pre = null;
        current = n;
        while (current != null) {
            next = current.next;
            current.next = pre;
            pre = current;
            current = next;
        }
        return current;
    }
}
