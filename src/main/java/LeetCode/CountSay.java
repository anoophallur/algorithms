package LeetCode;

/**
 * Created by anoop on 1/5/15.
 */
public class CountSay {
    public String countAndSay(int n) {
        // map = new HashMap<Integer, String>();
        // map.set(1, "1");
        // map.set(2, "11");
        // map.set(3, "11");
        if(n == 0){
            return "10";
        }
        return helper(n);
    }

    private String helper(int n){
        if(n == 1)
            return "11";
        String prev = helper(n-1);
        String result = nextGen(prev);

        // map.set(n, result);
        return result;
    }

    private String nextGen(String str){
        int l = str.length();
        char prev = str.charAt(0), present;
        int count = 1;
        StringBuilder sb = new StringBuilder();
        for(int i = 1 ; i < l; i++){
            present = str.charAt(i);
            // if(present == '-')continue;

            if(prev == present){
                count++;
            }else{
                sb.append(count);
                sb.append(prev);
                prev = present;
                count = 1;
            }
        }
        sb.append(count);
        sb.append(prev);
        return sb.toString();
    }
}
