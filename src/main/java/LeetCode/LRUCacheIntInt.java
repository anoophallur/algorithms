package LeetCode;

import java.util.HashMap;

public class LRUCacheIntInt {

    // For Double Linked List to keep track of accesses
    class Node{
        int key;
        int val;
        Node next;
        Node prev;
        public Node(int k, int v){
            key = k;
            val = v;
            next = null;
            prev = null;
        }
    }

    Node head;// most recently used             head <--> some1 <--> some2 <-.. . . -> some3 <--> tail
    Node tail;// least recently used   (most recently used)                              (least recently used)

    int capacity;

    HashMap<Integer, Node> map; // To maintain O(1) for get and set

    public LRUCacheIntInt(int capacity) {
        // Only initialization
        map = new HashMap<Integer, Node>();
        this.capacity = capacity;
        head = null;
        tail = null;
    }

    public void set(int key, int value) {
        // This will go into hash map as well as linked list.
        // If it already exists or there is no space, we have to remove it
        Node node = new Node(key, value);

        // Removing the node from hashmap + linked list if it already exists in it
        Node n = map.get(key);
        if(n != null){
            // Removing fom hashmap, not complicated
            map.remove(key);

            // correct the removed node neighbours pointers
            if(n.prev != null) n.prev.next = n.next;
            if(n.next != null) n.next.prev = n.prev;

            // update the head and tail if needed
            if(n == head) head=head.next;
            if(n == tail) tail=tail.prev;
        }

        // Removing the node due to full capacity
        if(map.size() >= capacity){
            // Removing the tail element in the linked list, as well as from hashmap
            map.remove(tail.key);

            // update the tail
            if(tail.prev != null)
                tail.prev.next = null;
            tail = tail.prev;
        }

        // Actual insertion. Culmination of above cases or independent
        if(head == null){ // This if block is executed only for the first set() call
            head = node;
            tail = node;
        }else{
            // When we insert an element, we update the head of the linked list to point to this element
            head.prev = node;
            node.next = head;
            head = node;
        }
        map.put(key, node);
    }

    public int get(int key) {
        Node node = map.get(key);
        // Simplest case. If its not there, return -1
        if(node == null){
            return -1;
        }else { // Have to update the head and return the value

            if(node != head) { // If its alread the head, do nothing. i.e its already the most recently accessed element

                //If its not , then remove it from the list i.e break its connections from its neighbours
                node.prev.next = node.next;

                // If its the tail also(node.next = null here), move the tail, else continue with altering the connections
                if(tail == node)
                    tail=tail.prev;
                else {
                    node.next.prev = node.prev;
                }
                // By now the element is removed from the linked list.

                // Insert it at the head of the list, and update head
                head.prev = node;
                node.next = head;
                node.prev = null;

                head = node;
            }

            return node.val;
        }
    }

}