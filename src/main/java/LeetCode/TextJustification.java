package LeetCode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anoop on 1/3/15.
 */
public class TextJustification {

    public List<String> fullJustify(String[] words, int L) {
        List<String> result = new ArrayList<String>();
        int num_of_words = words.length;
        // if(num_of_words == 1 && words[0].isEmpty()){
        //     StringBuilder sb = new StringBuilder();
        //     for(int i = 0 ; i < L ; i++)sb.append(" ");
        //     result.add(sb.toString());
        //     return result;
        // }
        int count_run = 0, count;
        int start = 0, end = 0;
        boolean first = true;
        for(int i = 0 ; i < num_of_words; i++){
            count = words[i].length();
            if(first){
                first = false;
            }else{
                count += 1;
            }

            count_run += count;
            if(count_run > L){
                String line = packToLine(words, start, end, L);
                result.add(line);
                count_run = count-1;
                start = i;
                end = i;
            }else{
                end = i;
            }
        }
        // for handling last line
        StringBuilder line = new StringBuilder();
        for(int i = start ; i <= end; i++){
            line.append(words[i]);
            if(i != end)line.append(" ");
        }

        while(line.length() != L){
            line.append(" ");
        }
        result.add(line.toString());
        return result;
    }

    private String packToLine(String[] words, int start, int end, int L){
        int len = 0, word_len;
        boolean first = true;
        int num_of_words = end-start+1;
        int spacing = 1;
        //int[] spaces = new int[words.length - 1];
        for(int i = start; i <= end; i++){
            word_len = words[i].length();
            if(first){
                //      spaces[i] = word_len;
                first = false;
            }else{
                word_len += 1;
                //    if(i != end) spaces[i] = spaces[i-1] + word_len + 1;
            }
            len += word_len;
        }


        int diff = L - len;
        while(diff != 0){
            if(diff < num_of_words)break;

            spacing++;
            diff = diff - (num_of_words);
        }

        StringBuilder line = new StringBuilder();
        StringBuilder spacessb = new StringBuilder();
        for(int i = 0; i < spacing; i++)spacessb.append(" ");
        String space = spacessb.toString();

        for(int i = start; i <= end; i++){
            line.append(words[i]);
            if(i != end){
                line.append(space);
                if(diff-- > 0){
                    line.append(" ");
                }
            }
        }
        while(line.length() < L){
            line.append(" ");
        }
        return line.toString();
    }

}
