package BackTracking;

/**
 * Created by anoop on 12/29/14.
 */
public class RatMaze {
    private int[][] maze;

    public RatMaze(int[][] maze) {
        this.maze = maze;
    }

    int paths = 0;
    public int solve(int startx, int starty){
        solveUtil(startx, starty);
        return paths;
    }

    private void solveUtil(int x, int y) {
        if(x == maze.length-1 && y == maze[0].length-1){
            paths++;
            return;
        }
        if(isValid(x,y)){
            maze[x][y] = 1;
            solveUtil(x+1,y);
            solveUtil(x-1,y);
            solveUtil(x,y+1);
            solveUtil(x,y-1);
            maze[x][y] = 0;
        }
    }

    private boolean isValid(int x, int y) {
        return x>=0 && x < maze.length && y >= 0 && y < maze[0].length && maze[x][y]!=1;
    }
}
