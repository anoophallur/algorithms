package BackTracking;

/**
 * Created by anoop on 12/30/14.
 */
public class SubsetSum {

    private int[] arr;
    private int possibilities;

    public int findNumbersOfSubsetsThatSumTo(int[] arr, int target){
        this.arr = arr;
        int[] aux = new int[arr.length];

        isPossible(aux, target);

        return possibilities;
    }

    private void isPossible(int[] aux, int remain) {
        if(remain == 0){
            possibilities++;
        }
        if(remain < 0) return;

        for(int i = 0 ; i < aux.length; i++){
            if(aux[i] == 1) continue;

            aux[i] = 1;
            isPossible(aux, remain-arr[i]);
            aux[i] = 0;
        }

    }
}
