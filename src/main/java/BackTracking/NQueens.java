package BackTracking;


// Returns in how many ways can N queens be placed in NxN chess grid
public class NQueens {
    private int size;
    int[] b;
    int possibilities;

    public NQueens(int size) {
        possibilities = 0;
        this.size = size;
        b = new int[size];
        for(int i = 0 ; i < size; i++)
            b[i] = -1;
    }

    public int getCount() {
        for(int i = 0 ; i < size; i++){
            b[0] = i;
            isPossible(size - 1);
            b[0] = -1;
        }
        return possibilities;
    }

    private void isPossible(int n) {
        if(n == 0){
            possibilities++;
            printBoard();
        }
        int row;
        for(row = 0 ; row < size && b[row] != -1; row++);// b[row] now contains -1 i.e row'th row is unfilled

        if(row >= size)return;

        for(int i = 0 ; i < size; i++){
            if(isValid(row, i)){
                b[row] = i;
                isPossible(n - 1);
                b[row] = -1;
            }
        }
    }

    private boolean isValid(int row, int col) {
        if(b[row] != -1)return false;
        for(int i = 0 ; i < size;i++){
            if(b[i] == col) return false;
            if(b[i] == -1) continue;
            if(Math.abs(row - i) ==  Math.abs(col - b[i])) return false;
        }
        return true;
    }

    private void printBoard() {
        System.out.println("Solution No " + possibilities);
        for(int j = 0 ; j < size; j++)
            System.out.print("----");
        System.out.println("-");
        for(int i = 0 ; i < size; i++){
            for(int j = 0 ; j < size; j++){
                char p = j == b[i] ? 'Q' : ' ';
                System.out.printf("| %s ", p);
            }
            System.out.println("|");
            for(int j = 0 ; j < size; j++)
                System.out.print("----");
            System.out.println("-");
        }
        System.out.println();
    }
}
