package BackTracking;

public class KnightsTour {
    private static final int N = 8;
    private int[][] chessBoard = new int[N][N];
    private int[] x_moves = new int[]{2,1,-1,-2,-2,-1, 1, 2};
    private int[] y_moves = new int[]{1,2, 2, 1,-1,-2,-2,-1};

    public KnightsTour() {
        this.chessBoard = new int[N][N];
        for(int i = 0 ; i < N; i++){
            for(int j = 0 ; j < N; j++){
                chessBoard[i][j] = -1;
            }
        }
    }

    public void solve(int startx, int starty){
        chessBoard[startx][starty] = 1;
        if(solveUtil(startx, starty, 1) == true){
            System.out.println("Success");
            printBoard();
        }else {
            System.out.println("Not possible");
        }
    }

    private void printBoard() {
        System.out.println("--------------------------------------------------");
        for(int i = 0 ; i < N; i++){
            for(int j = 0 ; j < N; j++){
                System.out.print(String.format("| %3d ", chessBoard[i][j]));
            }
            System.out.println(" |");
            System.out.println("--------------------------------------------------");
        }
    }

    private boolean solveUtil(int x, int y, int covered) {
        int new_x, new_y;
        if(covered == N*N)return true;
        for(int i = 0 ; i < N; i++){
            new_x = x + x_moves[i];
            new_y = y + y_moves[i];
            if(isValid(new_x, new_y)){
                chessBoard[new_x][new_y] = covered+1;
                if(solveUtil(new_x,new_y,covered+1))
                    return true;
                else {
                    chessBoard[new_x][new_y] = -1;
                }
            }
        }
        return false;
    }

    private boolean isValid(int x, int y) {
        return x >= 0 && x < N && y >= 0 && y < N && chessBoard[x][y] == -1;
    }

}
