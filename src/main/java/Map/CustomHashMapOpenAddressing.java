package Map;

public class CustomHashMapOpenAddressing<K,V> {

    private Entry[] table;
    private float loadFactor;
    private float reductionFactor;
    private int size;
    private int entries;

    public int size() {
        return entries;
    }

    private class Entry<K,V>{
        private K key;
        private V value;

        public Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
    private static final int INITIAL_SIZE = 2;

    public CustomHashMapOpenAddressing() {
        table = new Entry[INITIAL_SIZE];
        size = INITIAL_SIZE;
        loadFactor = 0.75f;
        reductionFactor = 0.5f;
        entries = 0;
    }

    public void put(K key, V value){
        if((float)(entries + 1)/ size > loadFactor){
            expandTable();
        }
        int pos =  internalHash(key.hashCode());
        while(table[pos] != null){
            pos = (pos+1)%size;
        }
        table[pos] = new Entry(key, value);
        entries++;
    }

    public V remove(K key){
        int pos = internalHash(key.hashCode());
        V value = null;
        while(table[pos] != null){
            Entry entry = table[pos];
            if(key.equals(entry.key)){
                value = (V)entry.value;
                table[pos] = null;
                entries--;
            }
            pos = (pos+1)%size;
        }
        if(size > INITIAL_SIZE && (float)(entries + 1)/ size < reductionFactor){
            shrinkTable();
        }
        return value;
    }

    private int internalHash(int val){
        return Math.abs(val)%size;
    }

    private void shrinkTable() {
        Entry[] newTable = new Entry[size/2];
        for(int i = 0 ; i < size;i++){
            if(table[i] != null){
                newTable[Math.abs(table[i].key.hashCode())%(size/2)] = table[i];
            }
        }
        table = newTable;
        size = size/2;

    }

    private void expandTable() {
        Entry[] newTable = new Entry[size*2];
        for(int i = 0 ; i < size;i++){
            if(table[i] != null){
                newTable[Math.abs(table[i].key.hashCode())%(size*2)] = table[i];
            }
        }
        table = newTable;
        size = size*2;

    }

}
