package HackerRank;

import java.util.*;
public class QuickSort {

    static void quickSort(int[] ar) {

        quickSortHelper(ar,0,ar.length-1);

        printArray(ar);
    }
    private static void quickSortHelper(int[] ar, int s, int e){
        if(e <= s) return;
        int pivot = partition(ar, s, e);
        //for(int i = s+1; i < pivot; i++){
        //  System.out.print(ar[i] + " ");
        //}
        //System.out.println("");
        quickSortHelper(ar, s, pivot-1);
        quickSortHelper(ar,pivot+1,e);
    }

    private static int partition(int[] ar, int s, int e){

        int[] temp = new int[e-s+1];
        int val = ar[s];
        int j = s;
        int k = 0;
        int pivot = 0;
        for(int i = s+1 ; i <= e ; i++){
            if(ar[i] < val)ar[j++] = ar[i];
            else temp[k++] = ar[i];
        }
        pivot = j;
        ar[j++] = val;
        k = 0;
        for(; j <= e ;j++, k++){
            ar[j] = temp[k];
        }
        return pivot;
    }

    static void printArray(int[] ar) {
        for(int n: ar){
            System.out.print(n+" ");
        }
        System.out.println("");
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] ar = new int[n];
        for(int i=0;i<n;i++){
            ar[i]=in.nextInt();
        }
        quickSort(ar);
    }
}
