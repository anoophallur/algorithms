package Codeforces;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Round288ProblemA {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String line = br.readLine();
        String[] splits = line.trim().split(" ");
        int rows = Integer.parseInt(splits[0]);
        int cols = Integer.parseInt(splits[1]);
        int k = Integer.parseInt(splits[2]);

        int r, c;
        int[][] grid = new int[rows][cols];
        boolean filled = false;
        for (int i = 1; i <= k && !filled; i++) {
            line = br.readLine();
            splits = line.trim().split(" ");
            r = Integer.parseInt(splits[0])-1;
            c = Integer.parseInt(splits[1])-1;
            if(!filled) {
                grid[r][c] = 1;
                filled = checkFilled(grid, rows, cols, r, c);
                if (filled) {
                    System.out.println(i);
                }
            }
        }
        if (!filled) {
            System.out.println(0);
        }
    }

    private static boolean checkFilled(int[][] grid, int rows, int cols, int r, int c) {
        int top = r == 0 ? 0 : grid[r - 1][c];
        int bot = r == rows-1 ? 0 : grid[r + 1][c];
        int left = c == 0 ? 0 : grid[r][c - 1];
        int right = c == cols-1 ? 0 : grid[r][c + 1];
        int topleft = r == 0 || c == 0 ? 0 : grid[r - 1][c - 1];
        int topright = r == 0 || c == cols-1 ? 0 : grid[r - 1][c + 1];
        int botleft = r == rows-1 || c == 0 ? 0 : grid[r + 1][c - 1];
        int botright = r == rows-1 || c == cols-1 ? 0 : grid[r + 1][c + 1];

        return ((top + left + topleft == 3) ||
                (top + right + topright == 3) ||
                (right + bot + botright == 3) ||
                (bot + left + botleft == 3));

    }
}
