package Codeforces;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by anoop on 4/4/15.
 */
public class ZeptoLab {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = br.readLine();
        int depth = Integer.parseInt(line);

        int numnodes = (2<<depth)-1;
        Node[] tree = new Node[numnodes+1];
        for(int i = 1 ; i <= numnodes; i++){
            tree[i] = new Node(0,0);
        }

        line = br.readLine();
        String[] splits = line.split(" ");
        for(int i = 0 ; i < splits.length; i=i+2){
            tree[i/2 + 1].leftlights = Integer.parseInt(splits[i]);
            tree[i/2 + 1].rightlights = Integer.parseInt(splits[i+1]);
        }
        int sum = 0;

        for(int i = numnodes/2 ; i > 0 ; i--){
            if(i*2 < numnodes/2){
                tree[i].leftlights = tree[i].leftlights + tree[i*2].leftlights;
            }
            if(i*2 + 1 < numnodes/2){
                tree[i].rightlights = tree[i].rightlights + tree[i*2 + 1].leftlights;
            }

            tree[i].diff = Math.abs(tree[i].leftlights - tree[i].rightlights);
            sum += tree[i].diff;
            if(tree[i].leftlights < tree[i].rightlights){
                tree[i].leftlights = tree[i].rightlights;
            } else {
                tree[i].rightlights = tree[i].leftlights;
            }
        }

        System.out.println(sum);
    }

    private static class Node {
        int leftlights;
        int rightlights;
        int diff;
        public Node(int leftlights, int rightlights){
            this.leftlights = leftlights;
            this.rightlights = rightlights;
            diff = 0;
        }
    }
}
