package StacksAndQuesues;

import java.util.Stack;

public class StackWithMax<T extends Comparable>{

    private Stack<Pair> table;

    private class Pair{
        T data;
        T max;

        Pair(T data, T max) {
            this.data = data;
            this.max = max;
        }
    }

    public StackWithMax() {
        this.table = new Stack<Pair>();
    }

    public boolean isEmpty(){
        return table.isEmpty();
    }

    public void push(T element){
        if(isEmpty()){
            table.push(new Pair(element, element));
            return;
        }
        T prevMax = table.peek().max;
        T newMax = prevMax.compareTo(element) > 0 ? prevMax : element;
        table.push(new Pair(element, newMax));
    }

    public T pop(){
        if(isEmpty()){
            System.out.println("Forbidden");
            return null;
        }
        return table.pop().data;
    }

    public T max(){
        return table.peek().max;
    }
}
