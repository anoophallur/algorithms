package GraphTry2;

/**
 * Created by anoop on 12/21/14.
 */
public class GNode<T> {
    T data;
    boolean visited;

    public GNode() {
        visited = false;
    }

    public GNode(T data) {
        this();
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GNode)) return false;

        GNode gNode = (GNode) o;

        if (visited != gNode.visited) return false;
//        if (!data.equals(gNode.data)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return data.hashCode();
    }
}
