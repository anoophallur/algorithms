package GraphTry2;

import java.util.*;

public class Graph<T> {
    // See the problem. If we use Set to denote the links, we wont have ordering gaurentee of the traversal.
    // So consecutive runs of DFS might produce different results
    private Map<GNode<T>, Set<GNode<T>>> nodes;

    public Graph() {
        nodes = new HashMap<GNode<T>, Set<GNode<T>>>();
    }

    public void addNode(GNode<T> node){
        if(nodes.containsKey(node)){
            System.out.println("Node already present, overwriting it");
        }
        HashSet<GNode<T>> nodeLinks = new HashSet<GNode<T>>();
        nodes.put(node, nodeLinks);
    }

    public void addEdge(GNode<T> node1, GNode<T> node2){
        if(!nodes.containsKey(node1) || !nodes.containsKey(node2)){
            System.err.println("No such nodes");
        }
        Set<GNode<T>> links1 = nodes.get(node1);
        links1.add(node2);
        Set<GNode<T>> links2 = nodes.get(node2);
        links2.add(node1);
    }

    public void doDfs(GNode<T> start){
        System.out.println(start.data);
        start.visited = true;
        Set<GNode<T>> links = nodes.get(start);
        for(GNode<T> link : links){
            if(link.visited == false){
                doDfs(link);
            }
        }
    }

    public void doBfs(GNode<T> start){
        List<GNode<T>> queue = new LinkedList<GNode<T>>();
        start.visited = true;
        queue.add(start);
        while(!queue.isEmpty()){
            GNode<T> workingNode = queue.remove(0);
            System.out.println(workingNode.data);
            Set<GNode<T>> links = nodes.get(workingNode);
            for(GNode<T> link : links){
                if(link.visited == false){
                    link.visited = true;
                    queue.add(link);
                }
            }
        }
    }
}
