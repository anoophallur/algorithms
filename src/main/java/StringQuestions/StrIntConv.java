package StringQuestions;

/**
 * Created by anoop on 12/25/14.
 */
public class StrIntConv {
    public String intToString(int num){
        boolean negative = false;
        StringBuffer res = new StringBuffer();
        res.append("");
        if(num < 0){
            negative = true;
            num = -num;
        }
        while (num > 0){
            res.append(num%10);
            num/=10;
        }
        if(negative)res.append("-");
        if(res.length() == 0){
            res.append("0");
        }
        return res.reverse().toString();
    }
}
