package StringQuestions;

/**
 * Created by anoop on 1/5/15.
 */
public class ReverseWords {
    public String reverseWords(String s) {
        int l = s.length();
        int en = -1;
        int st = -1;
        char c;
        boolean first = true;
        StringBuilder sb = new StringBuilder();

        for(int i = l-1 ; i >=0 ;  i--){
            c = s.charAt(i);
            if(c == ' '){
                if(en == -1)
                    continue;
                else{
                    st = i+1;
                    if(first == false){
                        sb.append(" ");
                    }
                    first = false;
                    sb.append(s.substring(st, en));
                    en = -1;
                    st = -1;
                }
            }
            else{
                if(en == -1) en = i+1;
            }
        }
        if(en != -1){
            if(first == false)sb.append(" ");
            sb.append(s.substring(0,en));
        }


        return sb.toString();
    }
}
