package StringQuestions;

import java.util.Stack;

/**
 * Created by anoop on 1/1/15.
 */
public class ParanthesisMatching {
    public boolean isValid(String s) {
        Stack<Character> st = new Stack<Character>();
        Character c;
        for (int i = 0; i < s.length(); i++) {
            c = s.charAt(i);
            if (c.equals('{') || c.equals('(') || c.equals('[')) st.push(c);
            else if (c == '}') {
                if (st.isEmpty()) return false;
                else if (st.peek().equals('(')) st.pop();
            } else if (c == ']') {
                if (st.isEmpty()) return false;
                else if (st.peek().equals('[')) st.pop();
            } else if (c == ')') {
                if (st.isEmpty()) return false;
                else if (st.peek().equals('(')) st.pop();
            }
        }

        return st.isEmpty();
    }
}
