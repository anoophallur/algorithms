package StringQuestions;

public class ReverseString {
    public String doJob(String input){
        int length = input.length();
        if(length == 1) return input;

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(input.charAt(length-1));
        stringBuilder.append(doJob(input.substring(1,length-1)));
        stringBuilder.append(input.charAt(0));

        return stringBuilder.toString();
    }
}
