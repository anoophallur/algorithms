package StringQuestions;
import java.util.HashSet;
import java.util.Set;

public class StringPermutations {

    // HashSet ensures that duplicates in the string are handled as well
    private Set<String> permutations;

    public Set<String> getAllPermutation(String str){
        this.permutations = new HashSet<String>();
        permute("", str);
        return permutations;
    }

    private void permute(String prefix, String str) {
        int l = str.length();
        if(l == 0) {
            permutations.add(prefix);
        }else{
            for(int i = 0 ; i < l ; i++){
                permute(prefix+str.charAt(i), str.substring(0,i)+str.substring(i+1,l));
            }
        }

    }
}
