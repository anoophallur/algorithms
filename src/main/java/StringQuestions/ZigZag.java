package StringQuestions;

/**
 * Created by anoop on 1/5/15.
 */
public class ZigZag {
    public String convert(String s, int nRows) {
        int i = 0;
        int st = 0;
        int l = s.length();
        StringBuilder sb = new StringBuilder();
        if(nRows%2 != 0){
            for(int count = 0 ; count < l; count++){
                if(st + i >= l){
                    st = st + 1;
                    i = 0;
                }

                sb.append(s.charAt(st + i));

                if(st == nRows / 2 || st == nRows / 2 - 1)i = i + nRows/2 + 1;
                else i = i + nRows + 1;
            }
        }else{
            for(int count = 0 ; count < l; count++){
                if(st + i >= l){
                    st = st + 1;
                    i = 0;
                }

                sb.append(s.charAt(st + i));

                if(st == (nRows / 2)  - 1)i = i + nRows/2 + 1;
                else i = i + nRows;
            }
        }
        return sb.toString();
    }
}
