package StringQuestions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by anoop on 12/25/14.
 */
public class GeneratePhoneMnemonic {
    private static Map<Character, List<String>> map;

    static {
        map = new HashMap<Character, List<String>>();
        map.put('0', new ArrayList<String>(){{add("0");}});
        map.put('1', new ArrayList<String>(){{add("1");}});
        map.put('2', new ArrayList<String>(){{add("A");add("B");add("C");}});
        map.put('3', new ArrayList<String>(){{add("D");add("E");add("F");}});
        map.put('4', new ArrayList<String>(){{add("G");add("H");add("I");}});
        map.put('5', new ArrayList<String>(){{add("J");add("K");add("L");}});
        map.put('6', new ArrayList<String>(){{add("M");add("N");add("O");}});
        map.put('7', new ArrayList<String>(){{add("P");add("Q");add("R");add("S");}});
        map.put('8', new ArrayList<String>(){{add("T");add("U");add("V");add("W");}});
        map.put('9', new ArrayList<String>(){{add("X");add("Y");add("Y");add("Z");}});
    }

    public List<String> generate(String phoneNumber){
        ArrayList<String> res = new ArrayList<String>();
        int l = phoneNumber.length();
        if(l == 0){
            return res;
        } else{
            Character c = phoneNumber.charAt(0);
            List<String> t = map.get(c);
            if(l == 1){
                for(String ts : t){
                    res.add(ts);
                }
                return res;
            }
            List<String> t1 = generate(phoneNumber.substring(1, l));
            for(String ts : t){
                for(String t1s: t1){
                    res.add(ts+t1s);
                }
            }
            return res;
        }

    }
    public List<String> generateWithoutRecursion(String phoneNumber){

        ArrayList<String> res = new ArrayList<String>();
        int l = phoneNumber.length();
        if(l == 0){
            return res;
        }

        Character c = phoneNumber.charAt(0);
        List<String> t = map.get(c);
        if(l == 1){
            for(String ts : t){
                res.add(ts);
            }
            return res;
        }

        List<String> t1 = generate(phoneNumber.substring(1, l));
        for(String ts : t){
            for(String t1s: t1){
                res.add(ts+t1s);
            }
        }
        return res;
    }
}
