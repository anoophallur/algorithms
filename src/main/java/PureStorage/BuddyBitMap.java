package PureStorage;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by anoop on 3/23/15.
 */
public class BuddyBitMap {
    private static final boolean ONE = true;
    private static final boolean ZERO = false;
    private boolean[] arr;
    private int max_size;
    private int min_size;
    private int len;
    private Set<Integer> allocated;
    private static final int DEFAULT_MIN_SIZE = 128;
    private static final int DEFAULT_MAX_SIZE = 1024;

    public BuddyBitMap() throws Exception {
        this(DEFAULT_MAX_SIZE, DEFAULT_MIN_SIZE);
    }

    public BuddyBitMap(int max_size, int min_size) throws Exception {
        if(max_size % min_size != 0){
            throw new Exception("min_size should be perfect factor of max_size");
        }
        this.max_size = max_size;
        this.min_size = min_size;
        int ratio = max_size/min_size;
        len = (2*ratio) -1;
        arr = new boolean[len];
        this.allocated = new HashSet<Integer>();
    }

    public int allocate(int size) throws Exception {
        if(max_size % size != 0){
            throw new Exception("size should be perfect factor of max_size");
        }
        int start = max_size/size - 1;
        int end = 2*start;
        for(int i = start; i <= end ; i++){
            if(isAvailable(i)){
                allocated.add(i);
                arr[i] = ONE;
                setDescendents(i, ONE);
                setRelavantAncestors(i, ONE);
                return i;
            }
        }
        return -1;
    }

    public boolean free(int id){
        if(!allocated.contains(id)){
            return false;
        }
        allocated.remove(id);
        arr[id] = ZERO;
        setDescendents(id, ZERO);
        setRelavantAncestors(id, ZERO);
        return true;
    }

    private boolean isAvailable(int idx) {
        if(arr[idx] != ZERO)
            return false;
        int leftchild = 2*idx + 1;
        int rightchild = 2*idx + 2;
        boolean left_result = true;
        boolean right_result = true;
        if(leftchild < len) left_result = isAvailable(leftchild);
        if(rightchild < len) right_result = isAvailable(rightchild);
        return left_result && right_result;

    }

    private void setDescendents(int i, boolean val) {
        int leftchild = 2*i + 1;
        int rightchild = 2*i + 2;
        if(leftchild < len){
            arr[leftchild] = val;
            setDescendents(leftchild, val);
        }
        if(rightchild < len){
            arr[rightchild] = val;
            setDescendents(rightchild, val);
        }
    }

    private void setRelavantAncestors(int i, boolean val) {
        while(i > 0){
            if((i % 2 == 1 && i+1 < len && arr[i+1] == val) || (i % 2 == 0 && arr[i-1] == val)){
                arr[(i-1)/2] = val;
                i = (i-1)/2;
            } else {
                break;
            }
        }

    }

}
