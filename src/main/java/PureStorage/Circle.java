package PureStorage;

public class Circle {
    public int[][] draw(int r){
        int[][] circ = new int[2*r+1][2*r+1];
        int x=0,y=r, d;
        int rsq = square(r);
        while(y>=x){
            setpoints(circ,r, x,y);
            d = 2*(square(x+1) - rsq) + square(y) + square(y-1);
            if(d > 0)
                y = y-1;
            x=x+1;
        }
        return circ;
    }

    int square(int val){
        return val*val;
    }

    private void setpoints(int[][] circ,int r, int x, int y) {
        circ[r+x][r+y] = 1;
        circ[r+y][r+x] = 1;
        circ[r+y][r-x] = 1;
        circ[r+x][r-y] = 1;
        circ[r-x][r-y] = 1;
        circ[r-y][r-x] = 1;
        circ[r-y][r+x] = 1;
        circ[r-x][r+y] = 1;
    }


}
