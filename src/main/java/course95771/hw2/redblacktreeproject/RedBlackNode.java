package course95771.hw2.redblacktreeproject;

/**
 * Created by anoop on 1/2/15.
 */
public class RedBlackNode {
    static int BLACK;
    static int RED;

    private String data;
    private RedBlackNode parent;
    private RedBlackNode leftChild;
    private RedBlackNode rightChild;

    public RedBlackNode(String data, int color, RedBlackNode p, RedBlackNode lc, RedBlackNode rc) {
        parent = p;
        leftChild = lc;
        rightChild = rc;

        BLACK = color == 0 ? 1: 0;
        RED = 1 - BLACK;

        this.data = data;
    }

    public int getColor() {
        return BLACK == 1 ? 0 : 1;
    }

    public String getData() {
        return data;
    }

    public RedBlackNode getLc() {
        return leftChild;
    }

    public RedBlackNode getRc(){
        return rightChild;
    }

    public void setColor(int color){
        BLACK = color == 0 ? 1: 0;
        RED = 1 - BLACK;
    }

    public void setData(String data){
        this.data = data;
    }

    public void setLc(RedBlackNode lc){
        leftChild = lc;
    }

    public void setP(RedBlackNode p){
        parent = p;
    }

    public void setRc(RedBlackNode rc){
        rightChild = rc;
    }

    public String toString(){
        return data;
    }
}
