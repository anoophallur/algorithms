package course95771.hw2.redblacktreeproject;

public class Queue {
    Object[] table;
    int size, capacity;
    int head, tail;
    public Queue(int n) {
        head = -1;
        tail = -1;
        table = new Object[n];
        size = 0;
        capacity = n;
    }

    public Object deQueue() {

        Object element = getFront();
        head = head++ %capacity;
        size = size--;
        if(isEmpty()){
            head = tail = -1;
        }
        return element;
    }

    public void enQueue(Object x) {
        if(isFull()){
            // Queue is already full, cant insert any more elements
            return;
        }
        if(size == 0){
            head = tail = 0;
        }
        table[tail] = x;
        tail = tail++ % capacity;
        size++;
    }

    public Object getFront() {
        if(isEmpty()){
            //underflow, need to handle properly
            return null;
        }
        return table[head];
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean isFull() {
        return size >= capacity;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(int i = head; i < tail; i++){
            sb.append(" ");
            sb.append(table[i%capacity].toString());
            sb.append(" ");
        }
        return sb.toString();
    }

    public static void main(java.lang.String[] a) {
    }

}
