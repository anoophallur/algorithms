package CodingExercise;

import java.util.HashMap;
import java.util.Map;

public class InMemoryDB {
    private Map<String, Integer> map;
    private Map<Integer, Integer> countMap;

    public InMemoryDB() {
        this.map = new HashMap<String, Integer>();
        this.countMap = new HashMap<Integer, Integer>();
    }

    public void set(String key, Integer value) {
        unset(key);
        map.put(key, value);
        if (!countMap.containsKey(value))
            countMap.put(value, 1);
        else {
            Integer valueCount = countMap.get(value);
            countMap.put(value, ++valueCount);
        }
    }

    public Integer get(String key) {
        if(map.containsKey(key))
            return map.get(key);
        else
            return null;
    }

    public void unset(String key) {
        if(!map.containsKey(key))
            return;

        Integer value = map.get(key);
        Integer valueCount = countMap.get(value);
        if(valueCount == 1){
            countMap.remove(value);
        } else{
            countMap.put(value, valueCount-1);
        }
        map.remove(key);
    }

    public int numEqualTo(Integer value) {
        if(countMap.containsKey(value))
            return countMap.get(value);
        else
            return 0;
    }
}
