package GraphTry2;

import org.junit.Test;

public class GraphTest {

    @Test
    public void shouldInitializeGraph(){
        Graph<Integer> graph = new Graph<Integer>();

        GNode<Integer> node1 = new GNode<Integer>(1);
        GNode<Integer> node2 = new GNode<Integer>(2);
        GNode<Integer> node3 = new GNode<Integer>(3);
        GNode<Integer> node6 = new GNode<Integer>(6);
        GNode<Integer> node4 = new GNode<Integer>(4);
        GNode<Integer> node5 = new GNode<Integer>(5);
        GNode<Integer> node9 = new GNode<Integer>(9);
        GNode<Integer> node7 = new GNode<Integer>(7);
        GNode<Integer> node8 = new GNode<Integer>(8);

        graph.addNode(node1);
        graph.addNode(node2);
        graph.addNode(node3);
        graph.addNode(node6);
        graph.addNode(node4);
        graph.addNode(node5);
        graph.addNode(node9);
        graph.addNode(node7);
        graph.addNode(node8);

        graph.addEdge(node1, node2);
        graph.addEdge(node1, node3);
        graph.addEdge(node1, node6);
        graph.addEdge(node2, node4);
        graph.addEdge(node2, node5);
        graph.addEdge(node3, node9);
        graph.addEdge(node3, node7);
        graph.addEdge(node6, node7);
        graph.addEdge(node6, node8);
        graph.addEdge(node4, node5);
        graph.addEdge(node5, node9);
        graph.addEdge(node5, node7);
        graph.addEdge(node5, node8);

        System.out.println("Graph initialized");
    }

    @Test
    public void shouldTraverseProperly(){
        Graph<Integer> graph = new Graph<Integer>();

        GNode<Integer> node1 = new GNode<Integer>(1);
        GNode<Integer> node2 = new GNode<Integer>(2);
        GNode<Integer> node3 = new GNode<Integer>(3);
        GNode<Integer> node6 = new GNode<Integer>(6);
        GNode<Integer> node4 = new GNode<Integer>(4);
        GNode<Integer> node5 = new GNode<Integer>(5);
        GNode<Integer> node9 = new GNode<Integer>(9);
        GNode<Integer> node7 = new GNode<Integer>(7);
        GNode<Integer> node8 = new GNode<Integer>(8);

        graph.addNode(node1);
        graph.addNode(node2);
        graph.addNode(node3);
        graph.addNode(node6);
        graph.addNode(node4);
        graph.addNode(node5);
        graph.addNode(node9);
        graph.addNode(node7);
        graph.addNode(node8);

        graph.addEdge(node1, node2);
        graph.addEdge(node1, node3);
        graph.addEdge(node1, node6);
        graph.addEdge(node2, node4);
        graph.addEdge(node2, node5);
        graph.addEdge(node3, node9);
        graph.addEdge(node3, node7);
        graph.addEdge(node6, node7);
        graph.addEdge(node6, node8);
        graph.addEdge(node4, node5);
        graph.addEdge(node5, node9);
        graph.addEdge(node5, node7);
        graph.addEdge(node5, node8);

        System.out.println("Graph initialized");
        System.out.println("Now attempting Depth first search");
        graph.doDfs(node1);
        System.out.println("Now attempting Breadth first search");
        graph.doBfs(node1);
    }
}