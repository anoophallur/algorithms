package PureStorage;

import org.junit.Test;

public class CircleTest {
    @Test
    public void shouldTestCircle(){
        Circle circle = new Circle();
        int[][] points = circle.draw(7);
        for(int i = 0 ; i < points.length ; i++){
            int[] line = points[i];
            for(int j = 0 ; j < line.length; j++){
                System.out.print(line[j] == 1 ? ". " : "  ");
            }
            System.out.println("");
        }
    }

}