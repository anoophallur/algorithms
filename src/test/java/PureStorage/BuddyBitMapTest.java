package PureStorage;

import org.junit.Test;

import static org.junit.Assert.*;

public class BuddyBitMapTest {
    @Test
    public void shouldCheckBasicFunctionality() throws Exception {
        BuddyBitMap buddyBitMap = new BuddyBitMap(1024, 256);

        assertTrue(buddyBitMap.allocate(1024) >= 0);
        assertEquals(-1,buddyBitMap.allocate(1024));
    }

    @Test
    public void shouldCheckAllocatingAndFreeFunctionality() throws Exception {
        BuddyBitMap buddyBitMap = new BuddyBitMap(1024, 256);

        assertTrue(buddyBitMap.allocate(512) >= 0);
        assertTrue(buddyBitMap.allocate(512) >= 0);
        assertFalse(buddyBitMap.free(0));
        assertTrue(buddyBitMap.free(1));
        assertTrue(buddyBitMap.allocate(256) >= 0);
        assertTrue(buddyBitMap.allocate(256) >= 0);
        assertFalse(buddyBitMap.allocate(256) >= 0);
        assertFalse(buddyBitMap.free(1));

    }

    @Test
    public void shouldCheckDeep() throws Exception {
        BuddyBitMap buddyBitMap = new BuddyBitMap(1024, 256);
        assertTrue(buddyBitMap.allocate(256) >= 0);
        assertTrue(buddyBitMap.allocate(256) >= 0);
        assertFalse(buddyBitMap.allocate(1024) >= 0);

    }

    @Test
    public void shouldFreeProperly() throws Exception {
        BuddyBitMap buddyBitMap = new BuddyBitMap(1024, 512);
        assertTrue(buddyBitMap.allocate(512) >= 0);
        assertTrue(buddyBitMap.allocate(512) >= 0);
        assertFalse(buddyBitMap.allocate(1024) >= 0);
        assertTrue(buddyBitMap.free(1));
        assertTrue(buddyBitMap.free(2));
        assertTrue(buddyBitMap.allocate(1024) >= 0);
    }
}