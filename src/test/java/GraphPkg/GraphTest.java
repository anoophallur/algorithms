package GraphPkg;

import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

public class GraphTest {

    @Test
    public void shouldInitializaGrapg(){
        Graph<Integer> integerGraph = new Graph<Integer>();

        GraphNode<Integer> root5 = new GraphNode<Integer>(5);
        GraphNode<Integer> node8 = new GraphNode<Integer>(8);
        GraphNode<Integer> node16 = new GraphNode<Integer>(16);
        GraphNode<Integer> node13 = new GraphNode<Integer>(13);
        GraphNode<Integer> node2 = new GraphNode<Integer>(2);
        GraphNode<Integer> node18 = new GraphNode<Integer>(18);

        root5.addLink(node8);
        root5.addLink(node16);
        root5.addLink(node13);
        node8.addLink(node16);
        node16.addLink(node18);
        node16.addLink(node13);
        node13.addLink(node2);

        integerGraph.setRoot(root5);
        System.out.println("Graph initialized properly");
    }

    @Test
    public void bfsTest(){
        Graph<Integer> integerGraph = new Graph<Integer>();

        GraphNode<Integer> root5 = new GraphNode<Integer>(5);
        GraphNode<Integer> node8 = new GraphNode<Integer>(8);
        GraphNode<Integer> node16 = new GraphNode<Integer>(16);
        GraphNode<Integer> node13 = new GraphNode<Integer>(13);
        GraphNode<Integer> node2 = new GraphNode<Integer>(2);
        GraphNode<Integer> node18 = new GraphNode<Integer>(18);

        root5.addLink(node8);
        root5.addLink(node16);
        root5.addLink(node13);
        node8.addLink(node16);
        node16.addLink(node18);
        node16.addLink(node13);
        node13.addLink(node2);
        HashMap<String, String> stringStringHashMap = new HashMap<String, String>();
        stringStringHashMap.put("skfdj", "kjdf");
        HashSet<Integer> integers = new HashSet<Integer>();
        Iterator<Integer> iterator = integers.iterator();
        while(iterator.hasNext()){
            Integer next = iterator.next();
        }

        integerGraph.setRoot(root5);
        integerGraph.bfsTraverse();
        new LinkedList<Integer>();
    }

}