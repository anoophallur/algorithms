package Sorting;

import Sorting.Exceptions.ArrayLengthMismatchException;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class BubbleSortTest {
    @Test
    public void shouldDoBubbleSortOfArrayOfIntegers() throws ArrayLengthMismatchException {
        Sort<Integer> sort = new BubbleSort<Integer>();
        Integer anArray[]  = {5,2,4,6,1,3};

        sort.sortAscending(anArray);
        assertArrayEquals(new Integer[]{1, 2, 3, 4, 5, 6}, anArray);
    }

}