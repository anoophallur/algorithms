package Sorting;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class QuickSort_simpleTest {
    @Test
    public void shouldSort(){
        QuickSort_simple quickSort_simple = new QuickSort_simple();
        int[] arr = new int[]{2,4,3,2,9,5};
        quickSort_simple.sort(arr);
        assertArrayEquals(new int[]{2,2,3,4,5,9},arr);
    }

    @Test
    public void shouldTestiterativeVersion(){
        QuickSort_simple quickSort_simple = new QuickSort_simple();
        int[] arr = new int[]{2,4,3,2,9,5};
        quickSort_simple.iterativeQuickSort(arr);
        assertArrayEquals(new int[]{2,2,3,4,5,9},arr);
    }
}