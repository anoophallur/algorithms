package Sorting;

import Sorting.Exceptions.ArrayLengthMismatchException;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class InsertionSortTest {

    @Test
    public void shouldDoInsertionSortOfArrayOfIntegers() throws ArrayLengthMismatchException {
        Sort<Integer> sort = new InsertionSort<Integer>();
        Integer anArray[]  = {5,2,4,6,1,3};

        sort.sortAscending(anArray);
        assertArrayEquals(new Integer[]{1, 2, 3, 4, 5, 6}, anArray);
    }

    @Test
    public void shouldDoInsertionSortOfArrayOfCharacters() throws ArrayLengthMismatchException {
        Sort<Character> sort = new InsertionSort<Character>();
        Character anArray[]  = {'c','f','a','d','a','z'};

        sort.sortAscending(anArray);
        assertArrayEquals(new Character[]{'a', 'a', 'c', 'd', 'f', 'z'}, anArray);
    }

}