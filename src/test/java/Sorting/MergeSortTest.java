package Sorting;

import Sorting.Exceptions.ArrayLengthMismatchException;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class MergeSortTest{
    @Test
    public void shouldDoMergeSortOfArrayOfIntegers() throws ArrayLengthMismatchException {
        Sort<Integer> sort = new MergeSort<Integer>();
        Integer anArray[]  = {5,2,4,6,1,3};

        sort.sortAscending(anArray);
        assertArrayEquals(new Integer[]{1, 2, 3, 4, 5, 6}, anArray);
    }

    @Test
    public void shouldDoMergeSortOfArrayOfIntegersLengthOne() throws ArrayLengthMismatchException {
        Sort<Integer> sort = new MergeSort<Integer>();
        Integer anArray[]  = {5};

        sort.sortAscending(anArray);
        assertArrayEquals(new Integer[]{5}, anArray);
    }

    @Test
    public void shouldDoMergeSortOfArrayOfIntegersLengthTwo() throws ArrayLengthMismatchException {
        Sort<Integer> sort = new MergeSort<Integer>();
        Integer anArray[]  = {7, 5};

        sort.sortAscending(anArray);
        assertArrayEquals(new Integer[]{5, 7}, anArray);
    }

    @Test
    public void shouldDoMergeSortOfArrayOfIntegersLengthFour() throws ArrayLengthMismatchException {
        Sort<Integer> sort = new MergeSort<Integer>();
        Integer anArray[]  = {7, 5, 10, 8};

        sort.sortAscending(anArray);
        assertArrayEquals(new Integer[]{5, 7, 8, 10}, anArray);
    }

}