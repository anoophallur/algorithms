package Sorting;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class MergeSort_simpleTest {
    @Test
    public void shouldSort(){
        MergeSort_simple mergeSort_simple = new MergeSort_simple();
        int[] arr = new int[]{2,4,3,2,9,5};
        mergeSort_simple.sort(arr);
        assertArrayEquals(new int[]{2,2,3,4,5,9},arr);

    }
}