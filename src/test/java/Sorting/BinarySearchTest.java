package Sorting;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BinarySearchTest {
    @Test
    public void shouldSearchAnElement(){
        BinarySearch bs = new BinarySearch();

        assertEquals(2, bs.search(new int[]{3, 5, 8, 9, 10, 13}, 8));
        assertEquals(0, bs.search(new int[]{3}, 3));
        assertEquals(-1, bs.search(new int[]{3}, 8));
        assertEquals(-1, bs.search(new int[]{3}, 2));
        assertEquals(1, bs.search(new int[]{3,4}, 4));
    }
}