package Sorting;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class InsertionSort_SimpleTest {
    @Test
    public void shouldDoInsertionSort(){
        int[] arr = {12, 17, 5, 4, 8};
        InsertionSort_Simple is = new InsertionSort_Simple();
        is.sort(arr);
        assertArrayEquals(new int[]{4,5,8,12,17}, arr);
    }
}