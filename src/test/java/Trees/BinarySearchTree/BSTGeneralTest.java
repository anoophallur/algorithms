package Trees.BinarySearchTree;

import org.junit.Test;

import java.util.Stack;

import static org.junit.Assert.assertEquals;

public class BSTGeneralTest {

    @Test
    public void shouldDoGeneralAdditionAndDeletions(){
        try {
            BST<Integer> integerBST = new BST<Integer>();

            integerBST.setRoot(new BSTNode<Integer>(23));

            integerBST.addElement(new BSTNode<Integer>(30));
            integerBST.addElement(new BSTNode<Integer>(14));

            integerBST.bfsTraverse();
            integerBST.addElement(new BSTNode<Integer>(34));
            integerBST.addElement(new BSTNode<Integer>(12));
            integerBST.addElement(new BSTNode<Integer>(17));


            integerBST.removeElementWithValue(14);
            System.out.println("Aftter removing 14");
            System.out.println(integerBST.search(14));
        } catch (Exception genericException){
            genericException.printStackTrace();
        }
    }

    @Test
    public void depthTest(){
        try {
            BST<Integer> integerBST = new BST<Integer>();

            integerBST.addElement(new BSTNode<Integer>(5));
            integerBST.addElement(new BSTNode<Integer>(7));
            integerBST.addElement(new BSTNode<Integer>(2));
            integerBST.addElement(new BSTNode<Integer>(11));
            integerBST.addElement(new BSTNode<Integer>(3));
            integerBST.addElement(new BSTNode<Integer>(4));

            assertEquals(4, integerBST.maxDepth());

            Stack<Integer> stack = new Stack<Integer>();
        } catch (Exception genericException){
            genericException.printStackTrace();
        }
    }
}