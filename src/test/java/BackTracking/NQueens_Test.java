package BackTracking;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NQueens_Test {
    @Test
    public void shouldGetCorrectAnswer(){
        NQueens nQueens_ = new NQueens(4);
        int count = nQueens_.getCount();
        assertEquals(2, count);
    }
}