package BackTracking;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RatMazeTest {
    @Test
    public void shouldSolveRatmaze() {
        RatMaze ratMaze = new RatMaze(new int[][]{
                {0, 0, 1, 0, 0, 1, 0},
                {1, 0, 1, 1, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 1},
                {1, 0, 1, 0, 0, 0, 0},
                {1, 0, 1, 1, 0, 1, 0},
                {1, 0, 0, 0, 0, 1, 0},
                {1, 1, 1, 1, 0, 0, 0}
        });

        int paths = ratMaze.solve(0, 0);

        assertEquals(4,paths);
    }
}