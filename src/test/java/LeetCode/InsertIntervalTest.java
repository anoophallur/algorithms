package LeetCode;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class InsertIntervalTest {

    @Test
    public void testInsert() throws Exception {
        InsertInterval insertInterval = new InsertInterval();
        ArrayList<InsertInterval.Interval> intervals = new ArrayList<InsertInterval.Interval>();
        intervals.add(new InsertInterval.Interval(1,3));
        intervals.add(new InsertInterval.Interval(6,9));
        List<InsertInterval.Interval> insert = insertInterval.insert(intervals, new InsertInterval.Interval(2, 5));
        System.out.println(insert);
    }

    @Test
    public void testInsert2() throws Exception {
        InsertInterval insertInterval = new InsertInterval();
        ArrayList<InsertInterval.Interval> intervals = new ArrayList<InsertInterval.Interval>();
        intervals.add(new InsertInterval.Interval(1,2));
        intervals.add(new InsertInterval.Interval(3,5));
        intervals.add(new InsertInterval.Interval(6,7));
        intervals.add(new InsertInterval.Interval(8,10));
        intervals.add(new InsertInterval.Interval(12,16));
        List<InsertInterval.Interval> insert = insertInterval.insert(intervals, new InsertInterval.Interval(4, 9));
        System.out.println(insert);
    }
}