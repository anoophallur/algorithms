package LeetCode;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FractionToDecimalTest {

    @Test
    public void testFractionToDecimal() throws Exception {
        FractionToDecimal fractionToDecimal = new FractionToDecimal();
//        assertEquals("0.(3)",fractionToDecimal.fractionToDecimal(1,3));
//        assertEquals("-0.58(3)", fractionToDecimal.fractionToDecimal(7, -12));
//        assertEquals("0.0000000004656612873077392578125", fractionToDecimal.fractionToDecimal(-1, -2147483648));
        assertEquals("-2147483648", fractionToDecimal.fractionToDecimal(-2147483648, 1));
    }
}