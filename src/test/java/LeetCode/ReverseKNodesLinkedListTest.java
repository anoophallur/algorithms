package LeetCode;

import org.junit.Test;

public class ReverseKNodesLinkedListTest {

    @Test
    public void testReverseKGroup() throws Exception {
        ReverseKNodesLinkedList reverseKNodesLinkedList = new ReverseKNodesLinkedList();
        ReverseKNodesLinkedList.ListNode head = new ReverseKNodesLinkedList.ListNode(1);
        ReverseKNodesLinkedList.ListNode h2 = new ReverseKNodesLinkedList.ListNode(2);
        ReverseKNodesLinkedList.ListNode h3 = new ReverseKNodesLinkedList.ListNode(3);
        ReverseKNodesLinkedList.ListNode h4 = new ReverseKNodesLinkedList.ListNode(4);
        ReverseKNodesLinkedList.ListNode h5 = new ReverseKNodesLinkedList.ListNode(5);
        head.next = h2;
        h2.next = h3;
        h3.next = h4;
        h4.next = h5;


        reverseKNodesLinkedList.reverseKGroup(head, 2);
        System.out.println(head);
    }
}