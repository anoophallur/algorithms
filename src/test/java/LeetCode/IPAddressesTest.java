package LeetCode;

import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class IPAddressesTest {

    @Test
    @Ignore("Dont yet know how to solve this")
    public void testRestoreIpAddresses() throws Exception {
        IPAddresses ipAddresses = new IPAddresses();

        List<String> ips = ipAddresses.restoreIpAddresses("0000");
        assertEquals(1, ips.size());
    }
}