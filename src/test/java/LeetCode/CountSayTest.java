package LeetCode;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CountSayTest {

    @Test
    @Ignore("Dont yet know how to solve this")
    public void testCountAndSay() throws Exception {
        CountSay countSay = new CountSay();
//        assertEquals("1", countSay.countAndSay(0));
        assertEquals("1", countSay.countAndSay(1));
        assertEquals("11", countSay.countAndSay(2));
        assertEquals("21", countSay.countAndSay(3));
        assertEquals("1211", countSay.countAndSay(4));
        assertEquals("111221", countSay.countAndSay(5));
    }
}