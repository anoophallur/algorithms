package LeetCode;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class MergeIntervalTest {
    @Test
    public void shouldDoSomethine(){
        MergeInterval mergeInterval = new MergeInterval();
        MergeInterval.Interval interval1 = new MergeInterval.Interval(1,4);
        MergeInterval.Interval interval2 = new MergeInterval.Interval(1,4);

        ArrayList<MergeInterval.Interval> intervals = new ArrayList<MergeInterval.Interval>();
        intervals.add(interval1);
        intervals.add(interval2);

        List<MergeInterval.Interval> merge = mergeInterval.merge(intervals);

        System.out.println(merge);

    }

}