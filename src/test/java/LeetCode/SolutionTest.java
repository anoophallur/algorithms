package LeetCode;

import Trees.Solution;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class SolutionTest {
    @Test
    public void shoutTestGenerateClass() throws IOException {
        Solution solution = new Solution();
        String generatedText = solution.generateClass("/home/anoop/Workspace/Algorithms/src/main/java/Trees/Acoount.ji\n");
        String expectedText =
                "public String getName(){\n" +
                        "    return name;\n" +
                        "}\n" +
                        "public long getKey(){\n" +
                        "    return key;\n" +
                        "}\n" +
                        "public long getLastLoginTime(){\n" +
                        "    return lastLoginTime;\n" +
                        "}\n" +
                        "public void setLastLoginTime(long lastLoginTime){\n" +
                        "    this.lastLoginTime = lastLoginTime;\n" +
                        "}\n";
        assertEquals(expectedText, generatedText);
    }
}