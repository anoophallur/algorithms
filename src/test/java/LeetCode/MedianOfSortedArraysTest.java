package LeetCode;

import org.junit.Test;

public class MedianOfSortedArraysTest {
    @Test
    public void shouldTest(){
        MedianOfSortedArrays medianOfSortedArrays = new MedianOfSortedArrays();
        double medianSortedArrays = medianOfSortedArrays.findMedianSortedArrays(new int[]{1,2}, new int[]{2,3,4,5,6,7,8});
        System.out.println(medianSortedArrays);
    }
}