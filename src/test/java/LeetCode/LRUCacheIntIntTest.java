package LeetCode;

import org.junit.Test;

public class LRUCacheIntIntTest {

    @Test
    public void shouldTestFunctionalitu() {
        LRUCacheIntInt lruCacheIntInt = new LRUCacheIntInt(2);
        lruCacheIntInt.set(2, 1);
        lruCacheIntInt.set(3, 2);
        lruCacheIntInt.get(3);
        lruCacheIntInt.get(2);
        lruCacheIntInt.set(4, 3);
        lruCacheIntInt.get(2);
        lruCacheIntInt.get(3);
        lruCacheIntInt.get(4);
    }

    @Test
    public void shouldTestFunctionalitu4() {
        LRUCacheIntInt lruCacheIntInt = new LRUCacheIntInt(2);
        lruCacheIntInt.get(2);
        lruCacheIntInt.set(2, 6);
        lruCacheIntInt.get(1);
        lruCacheIntInt.set(1, 5);
        lruCacheIntInt.set(1, 2);
        lruCacheIntInt.get(1);
        lruCacheIntInt.get(2);
    }

    @Test
    public void shouldTest2() {
        LRUCacheIntInt lruCacheIntInt = new LRUCacheIntInt(2);
        lruCacheIntInt.set(2, 1);
        lruCacheIntInt.set(1, 1);
        lruCacheIntInt.set(2, 3);
        lruCacheIntInt.set(4, 1);
        lruCacheIntInt.get(1);
        lruCacheIntInt.get(2);
    }

    @Test
    public void shouldTest3() {
        LRUCacheIntInt lruCacheIntInt = new LRUCacheIntInt(10);
        lruCacheIntInt.set(10, 13);
        lruCacheIntInt.set(3, 17);
        lruCacheIntInt.set(6, 11);
        lruCacheIntInt.set(10, 5);
        lruCacheIntInt.set(9, 10);
        lruCacheIntInt.get(13);
        lruCacheIntInt.set(2, 19);
        lruCacheIntInt.get(2);
        lruCacheIntInt.get(3);
        lruCacheIntInt.set(5, 25);
        lruCacheIntInt.get(8);
        lruCacheIntInt.set(9, 22);
        lruCacheIntInt.set(5, 5);
        lruCacheIntInt.set(1, 30);
        lruCacheIntInt.get(11);
        lruCacheIntInt.set(9, 12);
        lruCacheIntInt.get(7);
        lruCacheIntInt.get(5);
        lruCacheIntInt.get(8);
        lruCacheIntInt.get(9);
        lruCacheIntInt.set(4, 30);
        lruCacheIntInt.set(9, 3);
        lruCacheIntInt.get(9);
        lruCacheIntInt.get(10);
        lruCacheIntInt.get(10);
        lruCacheIntInt.set(6, 14);
        lruCacheIntInt.set(3, 1);
        lruCacheIntInt.get(3);
        lruCacheIntInt.set(10, 11);
        lruCacheIntInt.get(8);
        lruCacheIntInt.set(2, 14);
        lruCacheIntInt.get(1);
        lruCacheIntInt.get(5);
        lruCacheIntInt.get(4);
        lruCacheIntInt.set(11, 4);
        lruCacheIntInt.set(12, 24);
        lruCacheIntInt.set(5, 18);
        lruCacheIntInt.get(13);
        lruCacheIntInt.set(7, 23);
        lruCacheIntInt.get(8);
        lruCacheIntInt.get(12);
        lruCacheIntInt.set(3, 27);
        lruCacheIntInt.set(2, 12);
        lruCacheIntInt.get(5);
        lruCacheIntInt.set(2, 9);
        lruCacheIntInt.set(13, 4);
        lruCacheIntInt.set(8, 18);
        lruCacheIntInt.set(1, 7);
        lruCacheIntInt.get(6);
        lruCacheIntInt.set(9, 29);
        lruCacheIntInt.set(8, 21);
        lruCacheIntInt.get(5);
        lruCacheIntInt.set(6, 30);
        lruCacheIntInt.set(1, 12);
        lruCacheIntInt.get(10);
        lruCacheIntInt.set(4, 15);
        lruCacheIntInt.set(7, 22);
        lruCacheIntInt.set(11, 26);
        lruCacheIntInt.set(8, 17);
        lruCacheIntInt.set(9, 29);
        lruCacheIntInt.get(5);
        lruCacheIntInt.set(3, 4);
        lruCacheIntInt.set(11, 30);
        lruCacheIntInt.get(12);
        lruCacheIntInt.set(4, 29);
        lruCacheIntInt.get(3);
        lruCacheIntInt.get(9);
        lruCacheIntInt.get(6);
        lruCacheIntInt.set(3, 4);
        lruCacheIntInt.get(1);
        lruCacheIntInt.get(10);
        lruCacheIntInt.set(3, 29);
        lruCacheIntInt.set(10, 28);
        lruCacheIntInt.set(1, 20);
        lruCacheIntInt.set(11, 13);
        lruCacheIntInt.get(3);
        lruCacheIntInt.set(3, 12);
        lruCacheIntInt.set(3, 8);
        lruCacheIntInt.set(10, 9);
        lruCacheIntInt.set(3, 26);
        lruCacheIntInt.get(8);
        lruCacheIntInt.get(7);
        lruCacheIntInt.get(5);
        lruCacheIntInt.set(13, 17);
        lruCacheIntInt.set(2, 27);
        lruCacheIntInt.set(11, 15);
        lruCacheIntInt.get(12);
        lruCacheIntInt.set(9, 19);
        lruCacheIntInt.set(2, 15);
        lruCacheIntInt.set(3, 16);
        lruCacheIntInt.get(1);
        lruCacheIntInt.set(12, 17);
        lruCacheIntInt.set(9, 1);
        lruCacheIntInt.set(6, 19);
        lruCacheIntInt.get(4);
        lruCacheIntInt.get(5);
        lruCacheIntInt.get(5);
        lruCacheIntInt.set(8, 1);
        lruCacheIntInt.set(11, 7);
        lruCacheIntInt.set(5, 2);
        lruCacheIntInt.set(9, 28);
        lruCacheIntInt.get(1);
        lruCacheIntInt.set(2, 2);
        lruCacheIntInt.set(7, 4);
        lruCacheIntInt.set(4, 22);
        lruCacheIntInt.set(7, 24);
        lruCacheIntInt.set(9, 26);
        lruCacheIntInt.set(13, 28);
        lruCacheIntInt.set(11, 26);
    }
}
