package LeetCode;

import org.junit.Test;

import java.util.List;

public class TextJustificationTest {

    @Test
    public void testFullJustify() throws Exception {
        TextJustification textJustification = new TextJustification();
        List<String> strings = textJustification.fullJustify(
                new String[]{"Listen","to","many,","speak","to","a","few."}, 6);
        System.out.println(strings);
    }
}