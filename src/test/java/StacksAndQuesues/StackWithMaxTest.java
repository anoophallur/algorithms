package StacksAndQuesues;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StackWithMaxTest {
    @Test
    public void shouldWorkProperly(){
        StackWithMax<Integer> integerStackWithMax = new StackWithMax<Integer>();

        integerStackWithMax.push(3);
        integerStackWithMax.push(5);
        integerStackWithMax.push(3);
        integerStackWithMax.push(2);

        assertEquals(new Integer(5), integerStackWithMax.max());
        assertEquals(new Integer(2), integerStackWithMax.pop());
        assertEquals(new Integer(3), integerStackWithMax.pop());
        assertEquals(new Integer(5), integerStackWithMax.pop());
        assertEquals(new Integer(3), integerStackWithMax.max());
    }
}