package Map;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CustomHashMapOpenAddressingTest {
    @Test
    public void shouldWorkProperlyAsHashMap(){
        CustomHashMapOpenAddressing<String, String> map =
                new CustomHashMapOpenAddressing<String, String>();

        map.put("Abu", "excellent homeworks");
        assertEquals(1, map.size());
        map.put("Magnus", "prodigy");
        assertEquals(2, map.size());
        map.put("Kesden", "crazy");
        assertEquals(3, map.size());
        map.put("Shruti", "childish");
        assertEquals(4, map.size());
        map.put("Sadique", "never meet");
        assertEquals(5, map.size());
        map.put("Eric", "genius");
        assertEquals(6, map.size());
        map.put("Anoop", "most useless");
        assertEquals(7, map.size());
        assertEquals("most useless", map.remove("Anoop"));
        assertEquals(6, map.size());
    }
}