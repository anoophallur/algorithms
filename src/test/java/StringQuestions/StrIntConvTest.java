package StringQuestions;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StrIntConvTest {

    @Test
    public void testIntToString() throws Exception {
        StrIntConv strIntConv = new StrIntConv();
        assertEquals("123", strIntConv.intToString(123));
    }
}