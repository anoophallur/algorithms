package StringQuestions;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ZigZagTest {

    @Test
    @Ignore("Dont yet know how to solve this")
    public void shouldConvertToZigZag(){
        ZigZag zigZag = new ZigZag();

        String actual = zigZag.convert("ABCD", 3);

        assertEquals("ABDC", actual);
    }

    @Test
    @Ignore("Dont yet know how to solve this")
    public void shouldConvertToZigZag2(){
        ZigZag zigZag = new ZigZag();

        String actual = zigZag.convert("ABCD", 2);

        assertEquals("ACBD", actual);
    }
}