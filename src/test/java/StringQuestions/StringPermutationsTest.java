package StringQuestions;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class StringPermutationsTest {

    @Test
    public void shouldGetAllPermuatations(){
        StringPermutations stringPermutations = new StringPermutations();
        Set<String> permutations = stringPermutations.getAllPermutation("abc");

        HashSet<String> expected = new HashSet<String>();
        expected.add("abc");
        expected.add("acb");
        expected.add("bac");
        expected.add("bca");
        expected.add("cab");
        expected.add("cba");
        System.out.println(permutations);
        assertEquals(expected, permutations);
    }
}