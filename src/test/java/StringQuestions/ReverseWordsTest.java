package StringQuestions;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ReverseWordsTest {

    @Test
    public void testReverseWords() throws Exception {
        ReverseWords reverseWords = new ReverseWords();
        String str = "what is your name";
        String expected = "name your is what";

        String actual = reverseWords.reverseWords(str);
        assertEquals(expected, actual);
    }
}