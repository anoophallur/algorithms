package StringQuestions;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ReverseStringTest {

    @Test
    public void shouldReverseString(){
        ReverseString reverseString = new ReverseString();
        String input = "abcdefghi";

        assertEquals("ihgfedcba",reverseString.doJob(input));
    }
}