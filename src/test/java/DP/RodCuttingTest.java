package DP;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RodCuttingTest {

    @Test
    public void testFindMaxPrice() throws Exception {
        RodCutting rodCutting = new RodCutting();

        assertEquals(22, rodCutting.findMaxPrice(new int[]{1, 5, 8, 9, 10, 17, 17, 20}, 8));
    }
}