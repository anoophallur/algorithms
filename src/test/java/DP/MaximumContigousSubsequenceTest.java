package DP;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class MaximumContigousSubsequenceTest {
    @Test
    public void shouldTestNaiveImplementation(){
        MaximumContigousSubsequence maximumContigousSubsequence = new MaximumContigousSubsequence();
        int[] mscs = maximumContigousSubsequence.usingNaiveApproach(new int[]{-2, -3, 4, -1, -2, 1, 5, -3});

        assertArrayEquals(new int[]{4, -1, -2, 1, 5}, mscs);
    }

    @Test
    public void shouldTestDPImplementation(){
        MaximumContigousSubsequence maximumContigousSubsequence = new MaximumContigousSubsequence();
        int[] mscs = maximumContigousSubsequence.usingDP(new int[]{-2, -3, 4, -1, -2, 1, 5, -3});

        assertArrayEquals(new int[]{4, -1, -2, 1, 5}, mscs);
    }

}