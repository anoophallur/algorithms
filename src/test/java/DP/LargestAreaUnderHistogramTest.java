package DP;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LargestAreaUnderHistogramTest {
    @Test
    public void shouldCompute(){
        assertEquals(6, new LargestAreaUnderHistogram().calculate(new int[]{2, 4, 2, 1}));
        assertEquals(18, new LargestAreaUnderHistogram().calculate(new int[]{2, 4, 2,1, 10,6,10}));
    }
}