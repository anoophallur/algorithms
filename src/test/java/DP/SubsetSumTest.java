package DP;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SubsetSumTest {
    @Test
    public void shouldTestSubsetSum(){
        SubsetSum subsetSum = new SubsetSum();
        assertTrue(subsetSum.isPossible(new int[]{3, 4, 5}, 8));
        assertTrue(subsetSum.isPossible(new int[] {3, 34, 4, 12, 5, 2}, 9));
    }
}