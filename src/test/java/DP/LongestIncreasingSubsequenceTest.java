package DP;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class LongestIncreasingSubsequenceTest {
    @Test
    public void shouldPerormCorrectlyForOOfNsquare(){
        LongestIncreasingSubsequence longestIncreasingSUbsequence = new LongestIncreasingSubsequence();
        int[] liss = longestIncreasingSUbsequence.usingOnsquare(new int[]{2, 7, 4, 9, 14, 7, 2, 3, 4, 5, 11, 10, 13, 2});

        assertArrayEquals(new int[]{2,3,4,5,10,13}, liss);

        int[] liss2 = longestIncreasingSUbsequence.usingOnsquare(new int[]{2, 6, 3, 4, 1, 2, 9, 5, 8});

        assertArrayEquals(new int[]{2,3,4,5,8}, liss2);
    }

    @Test
    @Ignore("Dont yet know how to solve this")
    public void shouldDoInNlgNtime(){
        LongestIncreasingSubsequence longestIncreasingSubsequence = new LongestIncreasingSubsequence();
        int[] lis = longestIncreasingSubsequence.usingOnlgn(new int[]{2, 6, 3, 4, 1, 2, 9, 5, 8});
        assertArrayEquals(new int[]{2,3,4,1,5}, lis);
    }
}