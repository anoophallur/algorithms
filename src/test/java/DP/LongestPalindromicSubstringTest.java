package DP;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LongestPalindromicSubstringTest {
    @Test
    public void shouldFindLengthOfLongestPalindromicSubsequence(){
        assertEquals(6, new LongestPalindromicSubstring().longestPalindromicSubstring("abccba"));
        assertEquals(4, new LongestPalindromicSubstring().longestPalindromicSubstring("abccb"));
    }
}