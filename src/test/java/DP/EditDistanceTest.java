package DP;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EditDistanceTest {

    @Test
    public void shouldFindEditDistance(){
        EditDistance editDistance = new EditDistance();
        int dist = editDistance.calculate("legant", "elegant");
        assertEquals(1,dist);
    }
}