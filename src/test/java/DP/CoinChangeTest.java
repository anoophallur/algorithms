package DP;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CoinChangeTest {

    @Test
    public void testNumberOfCombinations() throws Exception {
        CoinChange coinChange = new CoinChange();
        // {{1}}
        assertEquals(1, coinChange.numberOfCombinations(1, new int[]{25, 10, 5, 1}));

        assertEquals(1, coinChange.numberOfCombinations(2, new int[]{25, 10, 5, 1}));
        assertEquals(1, coinChange.numberOfCombinations(3, new int[]{25, 10, 5, 1}));
        assertEquals(1, coinChange.numberOfCombinations(4, new int[]{25, 10, 5, 1}));

        // {{1,1,1,1,1},{5}}
        assertEquals(2, coinChange.numberOfCombinations(5, new int[]{25, 10, 5, 1}));

        assertEquals(2, coinChange.numberOfCombinations(6, new int[]{25, 10, 5, 1}));
        assertEquals(2, coinChange.numberOfCombinations(7, new int[]{25, 10, 5, 1}));
        assertEquals(2, coinChange.numberOfCombinations(8, new int[]{25, 10, 5, 1}));
        assertEquals(2, coinChange.numberOfCombinations(9, new int[]{25, 10, 5, 1}));

        // {{1,1,1,1,1,1,1,1,1,1},{1,1,1,1,1,5},{5,5},{10}}
        assertEquals(4, coinChange.numberOfCombinations(10, new int[]{25, 10, 5, 1}));

        assertEquals(4, coinChange.numberOfCombinations(11, new int[]{25, 10, 5, 1}));
        assertEquals(4, coinChange.numberOfCombinations(12, new int[]{25, 10, 5, 1}));
        assertEquals(4, coinChange.numberOfCombinations(13, new int[]{25, 10, 5, 1}));
        assertEquals(4, coinChange.numberOfCombinations(14, new int[]{25, 10, 5, 1}));

        // {{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, {1,1,1,1,1,1,1,1,1,1,5},{1,1,1,1,1,5,5},{1,1,1,1,1,10}, {5,5,5},{20,5}}
        assertEquals(6, coinChange.numberOfCombinations(15, new int[]{25, 10, 5, 1}));

        assertEquals(6, coinChange.numberOfCombinations(16, new int[]{25, 10, 5, 1}));
        assertEquals(6, coinChange.numberOfCombinations(17, new int[]{25, 10, 5, 1}));
        assertEquals(6, coinChange.numberOfCombinations(18, new int[]{25, 10, 5, 1}));
        assertEquals(6, coinChange.numberOfCombinations(19, new int[]{25, 10, 5, 1}));

        // {{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,5},{1,1,1,1,1,1,1,1,1,1,5,5},
        // {1,1,1,1,1,1,1,1,1,1,10},{1,1,1,1,1,5,10},{5,5,5,5},{5,5,10}, {10,20},{20}}
        assertEquals(9, coinChange.numberOfCombinations(20, new int[]{25, 10, 5, 1}));

        assertEquals(9, coinChange.numberOfCombinations(21, new int[]{25, 10, 5, 1}));
        assertEquals(9, coinChange.numberOfCombinations(22, new int[]{25, 10, 5, 1}));
        assertEquals(9, coinChange.numberOfCombinations(23, new int[]{25, 10, 5, 1}));
        assertEquals(9, coinChange.numberOfCombinations(24, new int[]{25, 10, 5, 1}));

//        {
//        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
//        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,5},
//        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,5,5},
//        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,10},
//        {1,1,1,1,1,1,1,1,1,1,5,5,5},
//        {1,1,1,1,1,1,1,1,1,1,5,10},
//        {1,1,1,1,1,5,5,5,5},
//        {1,1,1,1,1,5,5,10},
//        {1,1,1,1,1,10,10},
//        {5,5,5,5,5},
//        {5,5,5,10},
//        {5,10,10},
//        {25},
//        }
        assertEquals(13, coinChange.numberOfCombinations(25, new int[]{25, 10, 5, 1}));

        assertEquals(13, coinChange.numberOfCombinations(26, new int[]{25, 10, 5, 1}));
        assertEquals(13, coinChange.numberOfCombinations(27, new int[]{25, 10, 5, 1}));
        assertEquals(13, coinChange.numberOfCombinations(28, new int[]{25, 10, 5, 1}));
        assertEquals(13, coinChange.numberOfCombinations(29, new int[]{25, 10, 5, 1}));


        assertEquals(242, coinChange.numberOfCombinations(100, new int[]{25, 10, 5, 1}));
    }
}