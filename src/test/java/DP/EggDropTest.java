package DP;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EggDropTest {
    @Test
    public void shouldGiveCorrectAnswer(){

        assertEquals(8, new EggDrop().eggDrop(2, 36));
        assertEquals(14, new EggDrop().eggDrop(2, 100));
    }
}