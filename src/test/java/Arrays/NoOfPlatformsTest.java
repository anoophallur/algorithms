package Arrays;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NoOfPlatformsTest {

    @Test
    public void shouldFindNumberOfPlatformsGivenArrivalTimeAndDepartureTime(){
        NoOfPlatforms noOfPlatforms = new NoOfPlatforms();
        int n = noOfPlatforms.findNumber(new int[]{1,3, 8, 9, 12}, new int[]{2,10, 11, 13, 14});

        assertEquals(3,n);
    }

}