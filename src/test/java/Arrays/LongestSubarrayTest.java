package Arrays;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LongestSubarrayTest {
    @Test
    public void shouldFindLenghtOfLongestSubarray(){
        LongestSubarray longestSubarray = new LongestSubarray();
        int length = longestSubarray.findLength(new int[]{3, 3, 3, 3, 4, 3, 3, 3, 3});
        assertEquals(4,length);
    }
}