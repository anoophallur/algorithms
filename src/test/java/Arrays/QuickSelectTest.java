package Arrays;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class QuickSelectTest {

    @Test
    public void testSelect() throws Exception {

        QuickSelect qs = new QuickSelect();
        assertEquals(5, qs.select(new int[]{24, 17, 12, 14, 31, 5, 20}, 0));
        assertEquals(12, qs.select(new int[]{24, 17, 12, 14, 31, 5, 20}, 1));
        assertEquals(14, qs.select(new int[]{24, 17, 12, 14, 31, 5, 20}, 2));
        assertEquals(17, qs.select(new int[]{24, 17, 12, 14, 31, 5, 20}, 3));
        assertEquals(20, qs.select(new int[]{24, 17, 12, 14, 31, 5, 20}, 4));
        assertEquals(24, qs.select(new int[]{24, 17, 12, 14, 31, 5, 20}, 5));
        assertEquals(31, qs.select(new int[]{24, 17, 12, 14, 31, 5, 20}, 6));
    }
}