package Arrays;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class DutchFlagTest {
    @Test
    public void shouldSortCorrectly(){
        DutchFlag dutchFlag = new DutchFlag();

        int[] a = {1, 3, 2, 2, 3, 1};
        dutchFlag.solve(a,2);
        assertArrayEquals(new int[]{1,1,2,2,3,3}, a);

        int[] b = {1,2,4,3,2,3,2,4,1,1,2,5,2,3};
        dutchFlag.solve(b,2);
        assertArrayEquals(new int[]{1,1,1,2,2,2,2,2,4,3,3,5,4,3}, b);
    }
}