Common Algorithms 
--------

> git clone https://anoophallur@bitbucket.org/anoophallur/algorithms.git
    
> cd algorithms

DEPENDENCIES: JAVA 1.7+(not sure with older versions)

First make sure all tests are correct by running, 
> ./gradlew test 

OR

> ./gradlew.bat test 

If all tests pass, then you can set up the project for your IDE. 
 
> ./gradlew idea [or eclipse] 

Add test cases and add new algorithms . . Goozd luck